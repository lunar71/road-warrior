#!/bin/bash
# -*- coding: utf8 -*-
set -e

export DEBIAN_FRONTEND=noninteractive

DEBIAN_FRONTEND=noninteractive sudo apt update -yqq
DEBIAN_FRONTEND=noninteractive sudo apt install -yqq x11vnc xvfb openbox

sudo bash -c 'cat > /etc/xdg/openbox/menu.xml << "EOF"
<?xml version="1.0" encoding="UTF-8"?>
<openbox_menu xmlns="http://openbox.org/"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://openbox.org/
                file:///usr/share/openbox/menu.xsd">
<menu id="root-menu" label="Openbox 3">
  <item label="Terminal emulator">
    <action name="Execute"><execute>x-terminal-emulator</execute></action>
  </item>
  <item label="Web browser">
    <action name="Execute"><execute>x-www-browser</execute></action>
  </item>
  <separator />
  <item label="ObConf">
    <action name="Execute"><execute>obconf</execute></action>
  </item>
  <item label="Reconfigure">
    <action name="Reconfigure" />
  </item>
  <item label="Restart">
    <action name="Restart" />
  </item>
  <separator />
  <item label="Exit">
    <action name="Exit" />
  </item>
</menu>
</openbox_menu>
EOF'

sudo sed -i 's/<name>Clearlooks<\/name>/<name>Nightmare<\/name>/g' /etc/xdg/openbox/rc.xml

cat > "${HOME}/start-x11vnc.sh" << "EOF"
#!/bin/bash

kill_xvfb() {
   ps aux | grep -i [x]vfb | awk '{print $2}' | xargs kill 
}

trap 'kill_xvfb' EXIT

x11vnc \
    -create \
    -env FD_PROG=/usr/bin/openbox-session \
    -env X11VNC_CREATE_GEOM="1280x1080x24" \
    -nopw \
    -norc \
    -listen 127.0.0.1 \
    -rfbport 5937 \
    -forever
EOF
chmod +x "${HOME}/start-x11vnc.sh"
