#!/bin/bash

if test $EUID -ne 0; then
    printf "%s\n" "[ERR] run as root"
    exit 1
fi

function usage() {
    printf "%s\n" \
        "usage: $(basename $0) [-s|--server <lan subnet>/<cidr>] [-c|--client] [-f|--force]" \
        "" \
        "-s|--server <lan subnet>/<cidr>    installs and configures tailscale as a subnet router" \
        "-c|--client <lan subnet>/<cidr>    installs tailscale as a client for a subnet router" \
        "-f|--force                         force reinstall"
    exit 1
}

function install() {
    if command -v apt-get &>/dev/null; then
        if ! command -v tailscale &>/dev/null; then
            printf "%s\n" "[INFO] installing dependencies"
            export DEBIAN_FRONTEND=noninteractive
            apt-get update -yqq &>/dev/null
            apt-get install -yqq curl &>/dev/null

            printf "%s\n" "[INFO] installing tailscale, log file /root/tailscale_install.log"
            curl -fsSL https://tailscale.com/install.sh | /bin/bash 2>&1 >/root/tailscale_install.log
        else
            printf "%s\n" \
                "[INFO] tailscale already installed" \
                "[INFO] use -f|--force to force reinstall"
            if test $force == true; then
                printf "%s\n" "[INFO] -f|--force supplied, reinstalling"
                printf "%s\n" "[INFO] installing dependencies"
                export DEBIAN_FRONTEND=noninteractive
                apt-get update -yqq &>/dev/null
                apt-get install -yqq curl &>/dev/null

                printf "%s\n" "[INFO] installing tailscale, log file /root/tailscale_install.log"
                curl -fsSL https://tailscale.com/install.sh | /bin/bash 2>&1 >/root/tailscale_install.log
            fi
        fi
    else
        printf "%s\n" "[ERR] non-ubuntu/non-debian not implemented"
        exit 1
    fi
    case $1 in
        server)
            if test -f /etc/sysctl.d/99-tailscale.conf; then
                if ! grep -q 'net.ipv4.ip_forward = 1' /etc/sysctl.d/99-tailscale.conf; then
                    printf "%s\n" "net.ipv4.ip_forward = 1" >> /etc/sysctl.d/99-tailscale.conf
                fi

                if ! grep -q 'net.ipv6.conf.all.forwarding = 1' /etc/sysctl.d/99-tailscale.conf; then
                    printf "%s\n" "net.ipv6.conf.all.forwarding = 1" >> /etc/sysctl.d/99-tailscale.conf
                fi
            fi
            sysctl -q -p /etc/sysctl.d/99-tailscale.conf
            tailscale up --advertise-routes="$2" --accept-routes
            ;;

        client)
            cat > /root/tailscale-client.sh << EOF
#!/bin/bash
case \$1 in
    up)   tailscale up --accept-routes --advertise-routes="$2";;
    down) tailscale down;;
    *)    printf "%s\\n" "usage: \$(basename \$0) up|down"; exit 1;;
esac

EOF
            chmod +x /root/tailscale-client.sh
            printf "%s\n" "[INFO] use /root/tailscale-client.sh to start the tailscale client"
            ;;
    esac
}

options=$(getopt -o s:c:fh --long server:,client:,force,help -n "$(basename $0)" -- "$@")
eval set -- "$options"
force=false
while true; do
    case "$1" in
        -s|--server) install "server" "$2"; break;;
        -c|--client) install "client" "$2"; break;;
        -f|--force)  force=true;;
        -h|--help)   usage; break;;
        *)           usage; break;;
    esac
    shift
done
