#!/bin/bash
# -*- coding: utf8 -*-
set -e

export DEBIAN_FRONTEND=noninteractive

DEBIAN_FRONTEND=noninteractive sudo apt update -yqq
DEBIAN_FRONTEND=noninteractive sudo apt install -yqq tigervnc-standalone-server tigervnc-common autocutsel

mkdir -p "${HOME}/.local/bin" &>/dev/null
cat > "${HOME}/.local/bin/vnc.sh" << "EOF"
#!/bin/bash

[[ -z $1 ]] && printf "%s\n" "Usage: $(basename $0) <start | stop | list>" && exit 1

case $1 in
    start)
        printf "%s\n" "Linux connection strings:" \
            "ssh -fL 5901:localhost:5901 $USER@$HOSTNAME sleep 10; vncviewer localhost:5901" \
            "vncviewer -via $USER@$HOSTNAME localhost::5901"

        printf "\n"

        printf "%s\n" "Windows connection strings:" \
            "PLINK.EXE -no-antispoof -N -L 5901:localhost:5901 $USER@$HOSTNAME" \
            "\"C:\\Program Files (x86)\\TigerVNC\\vncviewer.exe\" localhost:5901"

        vncserver -localhost yes -securitytypes none -cleanstale -rfbport 5901 &>/tmp/vnc.log
        ;;
    stop)
        for i in $(seq 1 10); do vncserver -kill :$i -clean; done
        ;;
    list)
        vncserver -list
        ;;
    *) printf "Usage: $(basename $0) <start | stop | list>\n"
        ;;
esac
EOF
chmod +x "${HOME}/.local/bin/vnc.sh"
