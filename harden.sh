#!/bin/bash
# https://blog.stribik.technology/2015/01/04/secure-secure-shell.html
# https://github.com/stribika/stribika.github.io/wiki/Secure-Secure-Shell
set -e

SSHD_CONF_FILE="/etc/ssh/sshd_config"
DATE="$(date -u --rfc-3339=seconds | sed 's/+00:00//' | tr ' ' '_' | tr '+:' '-')"

test "${EUID}" -ne 0 && printf "%s\n" "run as root" && exit 1

if ! command -v apt-get &>/dev/null; then
    printf "%s\n" "[ERR] distro not debian-based"
    exit 1
fi

export DEBIAN_FRONTEND=noninteractive
export DEBCONF_NOWARNINGS=yes

printf "%s\n" "[INFO] updating and installing packages"
apt-get update -yqq 2>&1 >/dev/null && \
    apt-get install --no-install-recommends -yqq unattended-upgrades tmux ufw 2>&1 >/dev/null && \
    apt-get autoremove --purge -yqq snap snapd 2>&1 >/dev/null && \
    apt-get clean 2>&1 >/dev/null && \
    apt-get autoclean 2>&1 >/dev/null

printf "%s\n" \
    "[INFO] installed unattended-upgrades, tmux, ufw" \
    "[INFO] removed snap, snapd"

>/etc/issue
>/etc/motd
printf "%s\n" "[INFO] cleared /etc/issue and /etc/motd"

rm -f /etc/cron.deny
rm -f /etc/at.deny
touch /etc/cron.allow /etc/at.allow
chown root:root /etc/cron.allow /etc/at.allow
chmod 0600 /etc/cron.allow /etc/at.allow
printf "%s\n" "[INFO] enabled root-only cron"

cp $SSHD_CONF_FILE{,.bak-$DATE}
cat << EOF | shuf > "${SSHD_CONF_FILE}"
Port                            22
Banner                          /etc/issue
UsePAM                          yes
Protocol                        2
Subsystem                       sftp    /usr/libexec/openssh/sftp-server
LogLevel                        quiet # change to verbose for more
PrintMotd                       no
AcceptEnv                       LANG LC_*
MaxSessions                     5
StrictModes                     yes
Compression                     no
MaxAuthTries                    3
IgnoreRhosts                    yes
PrintLastLog                    yes
AddressFamily                   inet
X11Forwarding                   no
PermitRootLogin                 yes
AllowTcpForwarding              no
ClientAliveInterval             1200
AllowAgentForwarding            no
PermitEmptyPasswords            no
ClientAliveCountMax             0
GSSAPIAuthentication            no
KerberosAuthentication          no
#PasswordAuthentication          no
IgnoreUserKnownHosts            yes
PermitUserEnvironment           no
ChallengeResponseAuthentication no
MACs                            hmac-sha2-512,hmac-sha2-256
Ciphers                         aes128-ctr,aes192-ctr,aes256-ctr
#MACs                            hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com
#Ciphers                         chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes128-ctr,aes192-ctr,aes256-ctr
#KexAlgorithms                   curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
#HostKey                         /etc/ssh/ssh_host_ed25519_key
#HostKey                         /etc/ssh/ssh_host_rsa_key
EOF

(
    cd /etc/ssh
    rm ssh_host_*key*
    ssh-keygen -t ed25519 -f ssh_host_ed25519_key -N "" < /dev/null
    ssh-keygen -t rsa -b 4096 -f ssh_host_rsa_key -N "" < /dev/null
)

printf "%s\n" "[INFO] hardened SSH configuration in /etc/ssh/sshd_config"

dpkg-reconfigure --priority=low unattended-upgrades
systemctl enable --now unattended-upgrades
printf "%s\n" "[INFO] enabled unattended-upgrades service"

sed -i '/GRUB_CMDLINE_LINUX=/ s/\"$/ ipv6.disable=1\"/; s/\" /\"/' /etc/default/grub && update-grub
printf "%s\n" "[INFO] disabled ipv6 in /etc/default/grub"

ufw allow 22/tcp
ufw --force enable
printf "%s\n" "[INFO] configured ufw to allow 22/tcp only"
