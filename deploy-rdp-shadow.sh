#!/bin/bash
# -*- coding: utf8 -*-
set -e

[[ $EUID -ne 0 ]] && printf "%s\n" "run as root" && exit 1

printf "%s\n" "installing freerdp-shadow-cli"
DEBIAN_FRONTEND=noninteractive apt-get update -yqq
DEBIAN_FRONTEND=noninteractive apt-get install -yqq freerdp2-shadow-x11 xinit

if [[ -f /etc/X11/Xwrapper.config ]]; then
    cp /etc/X11/Xwrapper.config /etc/X11/Xwrapper.config.bak
fi

sudo bash -c 'cat > /etc/X11/Xwrapper.config << EOF
allowed_users=anybody
needs_root_rights=yes
EOF'

cat > /opt/rdp-shadow.sh << "EOF"
#!/bin/bash
set -e

[[ $EUID -eq 0 ]] && printf "%s\n" \
    "don't run as sudo or root, X11 needs to start as current user" \
    "sudo password will be prompted when necessary" && exit 1

if pgrep -x "sddm" > /dev/null; then
    sudo systemctl -q stop sddm
    printf "%s\n" "stopping sddm"
# Check if lightdm is running
elif pgrep -x "lightdm" > /dev/null; then
    sudo systemctl -q stop lightdm
    printf "%s\n" "stopping lightdm"
# Check if gdm is running
elif pgrep -x "gdm" > /dev/null; then
    sudo systemctl -q stop gdm
    printf "%s\n" "stopping gdm"
fi

mkdir -p "${HOME}/rdp-shadow"

printf "%s\n" "sleeping for 60s to allow X11 to start"
nohup startx > ${HOME}/rdp-shadow/startx.log 2>&1 &
sleep 60
printf "%s\n" "started destop session, check ${HOME}/rdp-shadow/freerdp-shadow-cli.log"

nohup freerdp-shadow-cli /port:3389 /monitors:0 > "${HOME}/rdp-shadow/freerdp-shadow-cli.log" 2>&1 &
printf "%s\n" "started rdp shadow session, check ${HOME}/rdp-shadow/freerdp-shadow-cli.log log file"
EOF
chmod +x /opt/rdp-shadow.sh

printf "%s\n" "use /opt/rdp-shadow.sh to start a shadowing session (pre sddm/lightdm/gdm login)"

if [[ $(command -v ufw) ]]; then
    ufw allow 22/tcp &>/dev/null
    ufw allow 3389/tcp &>/dev/null
    ufw --force enable &>/dev/null
    printf "%s\n" "added 22/tcp and 3389/tcp to ufw"
fi
