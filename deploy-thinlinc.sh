#!/bin/bash
# -*- coding: utf-8 -*-
set -e
function _cleanup() { rm -rf "${temp}"; }
trap _cleanup EXIT

PORT="8443"

function usage() {
    cat << EOF
Setup ThinLinc server on debian-based Linux, preferably Ubuntu-based:
    - HTTPS web access port ${PORT} configured, disabled by default
    - ufw blocks 111, 904, 1010, 9000 and ${PORT}, only allows 22
    - use "-w" to enable WebAccess on ${PORT} and allow through firewall
    - deb/rpm/tarball/exe ThinLinc clients are downloaded to /opt/thinlinc
    - use clients instead of web access to only communicate through 22

Usage: $(basename $0) -i | -u | -d <kubuntu|xubuntu>
    -i      install and configure thinlinc, access on https://<ip>/<domain>:${PORT}
    -u      uninstall thinlinc
    -w      enable ThinLinc WebAccess service on ${PORT} and allow through firewall
    -r      remove all branding references (extra sec) (might break some html/css)
    -d      choose desktop package(s) to install:
                kubuntu|xubuntu|gnome|gnome-minimal|vanilla-gnome|mate|cinnamon|openbox
            install multiple with -d kubuntu -d xubuntu 
EOF
    exit 1
}

[[ $EUID -ne 0 ]] && printf "%s\n" "run as root" && exit 1
temp=$(mktemp -d)
packages=(python3 curl zip sendmail ufw)

function install_requirements() {
    printf "%s\n" "installing packages"
    DEBIAN_FRONTEND=noninteractive apt update -yqq
    #DEBIAN_FRONTEND=noninteractive apt upgrade -yqq
    DEBIAN_FRONTEND=noninteractive apt install -y ${packages[*]}
    DEBIAN_FRONTEND=noninteractive apt clean -yqq
    DEBIAN_FRONTEND=noninteractive apt autoclean -yqq
    DEBIAN_FRONTEND=noninteractive apt autoremove --purge -yqq
}

function download_thinlinc() {
    #URL="https://www.cendio.com/downloads/server/download.py"
    URL="https://www.cendio.com/downloads/server/tl-4.14.0-server.zip"
    printf "%s\n" "downloading thinlinc"
    curl -sSL $URL -o "${temp}/tl-latest.zip"

    if [[ -f "${temp}/tl-latest.zip" ]]; then
        printf "%s\n" "extracting thinlinc"
        unzip -q -o "${temp}/tl-latest.zip" -d "${temp}/tl"
    else
        printf "%s\n" "tl-latest.zip not found, exiting"
        exit 1
    fi

    mkdir -p /opt/thinlinc
    (cd /opt/thinlinc && \
        curl -sSLOJ 'https://www.cendio.com/downloads/clients/tl-4.14.0-2324-client-linux-dynamic-x86_64.tar.gz' && \
        curl -sSLOJ 'https://www.cendio.com/downloads/clients/thinlinc-client_4.14.0-2324_amd64.deb' && \
        curl -sSLOJ 'https://www.cendio.com/downloads/clients/thinlinc-client-4.14.0-2324.x86_64.rpm' && \
        curl -sSLOJ 'https://www.cendio.com/downloads/clients/tl-4.14.0_2324-client-macos.iso' && \
        curl -sSLOJ 'https://www.cendio.com/downloads/clients/tl-4.14.0-client-windows.exe' && \
        curl -sSLOJ 'https://www.cendio.com/downloads/clients/tl-4.14.0-clients.zip' && \
        cd -
    )
    
}

function install_thinlinc() {
    install_requirements
    download_thinlinc

    cd $temp/tl/*/packages
    dpkg --install --no-debsig --force-confold *.deb

    cat > /opt/thinlinc/answers.cfg << "EOF"
accept-eula=yes
server-type=master
migrate-conf=ignore
install-required-libs=yes
install-nfs=yes
install-sshd=yes
install-gtk=yes
install-python-ldap=yes
email-address=test@localhost.com
# admin
tlwebadm-password=$6$Z3g3/Hj13LxzlWsh$3FyxZqW7DfHIpNOSTAyWY.ZgTKWbbpHwt1Ho.jWpea9ZMnoOphU0l2bhISDS.8VQOb7SdNf7xkgzqCy3lxpfv/
setup-thinlocal=no
setup-nearest=no
setup-firewall=yes
setup-selinux=no
setup-apparmor=yes
missing-answer=ask
EOF

    /opt/thinlinc/sbin/tl-setup -a /opt/thinlinc/answers.cfg

    cat > /opt/thinlinc/etc/xstartup.d/00-xhost-localuser.sh << "EOF"
xhost +SI:localuser:$(id -un)
EOF
    chmod +x /opt/thinlinc/etc/xstartup.d/00-xhost-localuser.sh

    mkdir -p /etc/polkit-1/localauthority/50-local.d
    cat > /etc/polkit-1/localauthority/50-local.d/50-thinlinc-no-auth-dialogs.pkla << "EOF"
[Prevent auth dialogs in ThinLinc]
Identity=unix-user:*
Action=org.freedesktop.color-manager.create-device;org.freedesktop.color-manager.create-profile;org.freedesktop.color-manager.delete-device;org.freedesktop.color-manager.delete-profile;org.freedesktop.color-manager.modify-device;org.freedesktop.color-manager.modify-profile;org.debian.apt.update-cache
ResultAny=no
ResultInactive=no
ResultActive=yes
EOF

    cat > /etc/polkit-1/localauthority/50-local.d/51-thinlinc-no-network-dialogs.pkla << "EOF"
[Prevent NetworkManager dialogs in ThinLinc]
Identity=unix-user:*
Action=org.freedesktop.NetworkManager.settings.modify.system;org.freedesktop.NetworkManager.network-control
ResultAny=no
ResultInactive=no
ResultActive=yes
EOF
    
    cat > /etc/polkit-1/localauthority/50-local.d/52-thinlinc-allow-power-options.pkla << "EOF"
[Allow shutdown and reboot in ThinLinc]
Identity=unix-user:*
Action=org.freedesktop.login1.*;org.freedesktop.login1.*.suspend;org.freedesktop.login1.reboot;org.freedesktop.login1.reboot-multiple-sessions;org.freedesktop.login1.power-off;org.freedesktop.login1.power-off-multiple-sessions
ResultAny=yes
ResultInactive=yes
ResultActive=yes
EOF

    /opt/thinlinc/etc/tlwebaccess/make-dummy-cert $(hostname)
    #/opt/thinlinc/bin/tl-config "/vsmagent/agent_hostname=$(hostname)"
    /opt/thinlinc/bin/tl-config "/webaccess/listen_port=${PORT}"
    /opt/thinlinc/bin/tl-config "/profiles/show_intro=false"
    /opt/thinlinc/bin/tl-config "/sessionstart/background_color=#000000"

    systemctl enable --now -q vsmserver
    systemctl restart -q vsmserver
    systemctl enable --now -q vsmagent
    systemctl restart -q vsmagent

    systemctl disable --now -q tlwebadm

    if [[ $(command -v ufw) ]]; then

        if [[ $webaccess = true ]]; then
            ufw allow $PORT &>/dev/null
            systemctl enable --now -q tlwebaccess 
            systemctl restart -q tlwebaccess 
        else
            ufw deny $PORT &>/dev/null
            systemctl disable --now -q tlwebaccess 
        fi

        ufw allow 22 &>/dev/null
        ufw deny 1010 &>/dev/null
        ufw deny 9000 &>/dev/null
        ufw deny 111 &>/dev/null
        ufw deny 904 &>/dev/null
        ufw enable &>/dev/null
        ufw status
    else
        printf "%s\n" "WARNING: ufw not found, could not create firewall rules"
    fi

    rm -rf "${temp}"
}

function remove_all_branding() {
    for i in $(grep -lri --include='*.html' --include='*.tmpl' -e '<img.*badge.*' -e '<img.*thinlinc.*' /opt/thinlinc/*); do
        sed -i '/<img.*badge.*/I d' $i
        sed -i '/<img.*thinlinc.*/I d' $i
    done
    for i in $(grep -lri --include='*.html' --include='*.tmpl' '<title>[^<]*thinlinc[^<]*<\/title>' /opt/thinlinc/*); do
        sed -i '/<title>[^<]*thinlinc[^<]*<\/title>/I d' $i
    done
    for i in $(grep -lri --include='*.html' --include='*.tmpl' '<link rel.*icon.*' /opt/thinlinc/*); do
        sed -i '/<link rel.*icon.*/I d' $i
    done
    for i in $(grep -lri --include='*.html' --include='*.tmpl' '.*copyright.*' /opt/thinlinc/*); do
        sed -i '/.*copyright.*/I d' $i
    done
    for i in $(grep -lri --include='*.html' --include='*.tmpl' '.*version.*' /opt/thinlinc/*); do
        sed -i '/.*version.*/I d' $i
    done
    for i in $(grep -lri --include='*.html' --include='*.tmpl' --include='*.js' --include='*.css' 'thinlinc\|cendio' /opt/thinlinc/*); do
        sed -i 's/thinlinc\|cendio//gi' $i
    done

    systemctl restart -q tlwebaccess 
}

function configure_openbox() {
    cd /opt/thinlinc
    /opt/thinlinc/bin/tl-config "/profiles/openbox/xdg_session=openbox"
    /opt/thinlinc/bin/tl-config "/profiles/openbox/name=openbox"
    /opt/thinlinc/bin/tl-config "/profiles/order=unity gnome gnome-classic kde xfce cinnamon mate lxde openbox"
    printf "%s\n" "xsetroot -solid \"#303030\"" >> /etc/xdg/openbox/autostart

    cat > /etc/xdg/openbox/menu.xml << EOF
<?xml version="1.0" encoding="UTF-8"?>
<openbox_menu xmlns="http://openbox.org/"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://openbox.org/
                file:///usr/share/openbox/menu.xsd">
<menu id="root-menu" label="Openbox 3">
  <item label="Terminal emulator">
    <action name="Execute"><execute>x-terminal-emulator</execute></action>
  </item>
  <item label="Web browser">
    <action name="Execute"><execute>x-www-browser</execute></action>
  </item>
  <separator />
  <item label="ObConf">
    <action name="Execute"><execute>obconf</execute></action>
  </item>
  <item label="Reconfigure">
    <action name="Reconfigure" />
  </item>
  <item label="Restart">
    <action name="Restart" />
  </item>
  <separator />
  <item label="Exit">
    <action name="Exit" />
  </item>
</menu>
</openbox_menu>
EOF

}

function uninstall_thinlinc() {
    systemctl disable --now -q vsmserver
    systemctl disable --now -q vsmagent
    systemctl disable --now -q tlwebaccess 
    systemctl disable --now -q tlwebadm
    rm -rf /opt/thinlinc
}

webaccess=false
remove_branding=false

while getopts "iuwrd:h" ARG; do
    case "${ARG}" in
        i) action="install";;
        u) action="uninstall";;
        w) webaccess=true;;
        r) remove_branding=true;;
        d)
            case "${OPTARG}" in
                kubuntu)        packages+=("kubuntu-desktop");;
                xubuntu)        packages+=("xubuntu-desktop");;
                gnome)          packages+=("ubuntu-gnome-desktop");;
                gnome-minimal)  packages+=("ubuntu-desktop-minimal");;
                gnome-vanilla)  packages+=("vanilla-gnome-desktop");;
                mate)           packages+=("ubuntu-mate-desktop");;
                cinnamon)       packages+=("cinnamon-desktop-environment");;
                openbox)        packages+=("openbox rxvt firefox x11-xserver-utils");;
            esac
            desktop="${OPTARG}"
            ;;
        h) usage;;
        *) usage;;
    esac
done

[[ "${action}" != "install" && "${action}" != "uninstall" ]] && usage

case "${action}" in
    "install")
        install_thinlinc;;
    "uninstall")
        uninstall_thinlinc;;
esac

if [[ "${desktop}" == "openbox" ]]; then
    configure_openbox
fi

if [[ $remove_branding = true ]]; then
    remove_all_branding
fi
