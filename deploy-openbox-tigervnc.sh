#!/bin/bash
# -*- coding: utf8 -*-
set -e

#[[ -z $1 ]] && printf "Usage: $0 <max disconnect time (default 60)> <max idle time (default 75)>\n" && exit 1
#[[ -z $2 ]] && MaxDisconnectTime="60" || MaxDisconnectTime="$2"
#[[ -z $3 ]] && MaxIdleTime="75" || MaxIdleTime="$3"

export DEBIAN_FRONTEND=noninteractive

DEBIAN_FRONTEND=noninteractive sudo apt update -yqq
DEBIAN_FRONTEND=noninteractive sudo apt install -yqq tigervnc-standalone-server tigervnc-common autocutsel openbox rxvt firefox

mkdir -p $HOME/.vnc
chmod go-rwx $HOME/.vnc

sudo sed -i.bak 's/^#.*AllowTcpForwarding.*\|^#.*AllowTcpForwarding\ no/AllowTcpForwarding\ yes/' sshd_config
cat > $HOME/.vnc/xstartup << EOF
#!/bin/sh
unset SESSION_MANAGER
xsetroot -solid "#606060"
exec tint2 &
exec autocutsel -fork &
[ -s $HOME/.Xresources ] && xrdb .Xresources
[ -s /etc/X11/Xsession ] && exec /etc/X11/Xsession
#exec openbox-session &
#exec /usr/bin/startplasma-x11 &
#exec /usr/bin/xfce4-session &
EOF
chmod +x $HOME/.vnc/xstartup
sudo update-alternatives --set x-session-manager /usr/bin/openbox-session

mkdir -p "${HOME}/.local/bin" &>/dev/null
cat > "${HOME}/.local/bin/vnc.sh" << "EOF"
#!/bin/bash

[[ -z $1 ]] && printf "%s\n" "Usage: $(basename $0) <start | stop | list>" && exit 1

case $1 in
    start)
        printf "%s\n" "Linux connection strings:" \
            "ssh -fL 5901:localhost:5901 $USER@$HOSTNAME sleep 10; vncviewer localhost:5901" \
            "vncviewer -via $USER@$HOSTNAME localhost::5901"

        printf "\n"

        printf "%s\n" "Windows connection strings:" \
            "PLINK.EXE -no-antispoof -N -L 5901:localhost:5901 $USER@$HOSTNAME" \
            "\"C:\\Program Files (x86)\\TigerVNC\\vncviewer.exe\" localhost:5901"

        vncserver -localhost yes -securitytypes none -cleanstale -rfbport 5901 &>/tmp/vnc.log
        ;;
    stop)
        for i in $(seq 1 10); do vncserver -kill :$i -clean; done
        ;;
    list)
        vncserver -list
        ;;
    *) printf "Usage: $(basename $0) <start | stop | list>\n"
        ;;
esac
EOF
chmod +x "${HOME}/.local/bin/vnc.sh"

sudo loginctl enable-linger $USER
