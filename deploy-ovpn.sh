#!/bin/bash
# -*- coding: utf-8 -*-
#set -x

OPENVPN_ROOT="/etc/openvpn"
OPENVPN_SERVER_ROOT="${OPENVPN_ROOT}/server"
OPENVPN_EASYRSA_ROOT="${OPENVPN_SERVER_ROOT}/easy-rsa"
EASYRSA_RELEASE='https://github.com/OpenVPN/easy-rsa/releases/download/v3.1.2/EasyRSA-3.1.2.tgz'

if ! test "${EUID}" -eq 0; then
    printf "%s\n" "[ERR] run as root"
    exit 1
fi

if ! command -v host &>/dev/null; then
    printf "%s\n" "\"host\" not found"
    exit 1
fi

function install_usage() {
    cat << EOF
Usage: $(basename $0) -s <domain/ip> -p <port> -r <protocol> -d <dns> -o <domain name>

    -s      VPN server IP or domain; if \$OPTARG is domain, it will be resolved to IP
            this is the only mandatory argument as the rest have sane defaults

    -p      VPN port, default 31337

    -r      VPN protocol, TCP or UDP, default UDP

    -d      DNS resolvers to use, system|google|cloudflare|opendns, default google
            the system DNS resolver allows access into internal network hostnames

    -o      domain name to use, default '.'
            used in conjunction with -d system in order to access internal network

Example:
    $(basename $0) -s vpn.myserver.com -p 44435 -r udp -d google -o .
    $(basename $0) -s 10.20.30.40 -r udp -d cloudflare
    $(basename $0) -s vpn.myserer.com -r udp -d system -o myhome.lan
EOF
    exit 1
}

function client_usage() {
    cat << EOF
Usage: $(basename $0) [-a | -r <client name>] | -u (uninstall)

    -a      add a new client
    -r      revoke an existing client
    -l      list valid/revoked client certificates
    -u      boolean parameter to uninstall everything

Examples:
    $(basename $0) -a "client1"
    $(basename $0) -r "client2"
    $(basename $0) -l
    $(basename $0) -u
EOF
    exit 1
}

function setup_slack_notification() {
    cat > "${OPENVPN_SERVER_ROOT}/slack_notification.sh" << "EOF"
#!/bin/bash
case "${script_type}" in
    "client-connect")
	output="VPN connect";;
    "client-disconnect")
	output="VPN disconnect";;
    *)  
	output="VPN unknown (unknown \${script_type})";;
esac
/usr/bin/curl -X POST -H 'Authorization: Bearer xoxb-a-b-c' --data "channel=CHANGEME&text=$(date "+[%b %d %Y %H:%M:%S]") ${output} on $(hostname)" https://slack.com/api/chat.postMessage
EOF
    chmod +x "${OPENVPN_SERVER_ROOT}/slack_notification.sh"
    printf "%s\n" "[INFO] edit \"${OPENVPN_SERVER_ROOT}/slack_notification.sh\" and \"${OPENVPN_SERVER_ROOT}/server.conf\" to enable Slack connect/disconnect notifications"
}

function new_client() {
    read server_name server_port <<< $(grep -i 'remote ' "${OPENVPN_SERVER_ROOT}/client-common.txt" | awk '{print $2" "$3}')
    client_name="${client}_${server_name}-${server_port}.ovpn"
    cat > "/etc/openvpn/clients/${client_name}" << EOF
# ${client_name}
$(cat /etc/openvpn/server/client-common.txt)

# Server Certificate Authority
<ca>
$(cat /etc/openvpn/server/easy-rsa/pki/ca.crt)
</ca>

# Server PKI Certificate
<cert>
$(sed -ne '/BEGIN CERTIFICATE/,$ p' /etc/openvpn/server/easy-rsa/pki/issued/${client}.crt)
</cert>

# Client Private Key
<key>
$(cat /etc/openvpn/server/easy-rsa/pki/private/${client}.key)
</key>

# OpenVPN static Key
<tls-crypt>
$(sed -ne '/BEGIN OpenVPN Static key/,$ p' /etc/openvpn/server/tc.key)
</tls-crypt>
EOF
    chmod 0400 "/etc/openvpn/clients/${client_name}"
    printf "%s\n" \
        "[INFO] generated new client ${client_name}" \
        "[INFO] ovpn connection file is /etc/openvpn/clients/${client_name}"
}

function setup() {
    test -f /etc/os-release && . /etc/os-release || { printf "%s\n" "[ERR] could not identify distro from /etc/os-release"; exit 1; }
    ID="${ID,,}"
    VERSION_ID="${VERSION_ID//./}"

    case "${ID}" in
        "raspbian")
            export DEBCONF_NOWARNINGS=yes
            export DEBIAN_FRONTEND=noninteractive
            group_name="nogroup"
            packages="curl tar gnupg2 openvpn openssl ca-certificates iptables fail2ban"

            printf "%s\n" "[INFO] detected Raspbian ${VERSION_ID}"
            printf "%s\n" "[INFO] installing packages: ${packages}"

            apt-get update -yqq >/dev/null
            apt-get install -yqq $packages >/dev/null
            ;;
        "ubuntu")
            export DEBCONF_NOWARNINGS=yes
            export DEBIAN_FRONTEND=noninteractive
            group_name="nogroup"
            packages="curl tar gnupg2 openvpn openssl ca-certificates iptables fail2ban"

            printf "%s\n" "[INFO] detected Ubuntu ${VERSION_ID}"
            test "${VERSION_ID}" -lt 1804 && printf "%s\n" "[ERR] unsupported Ubuntu under version 1804, exiting" && exit 1
            printf "%s\n" "[INFO] installing packages: ${packages}"

            apt-get update -yqq >/dev/null
            apt-get install -yqq $packages >/dev/null
            ;;
        "debian")
            export DEBCONF_NOWARNINGS=yes
            export DEBIAN_FRONTEND=noninteractive
            group_name="nogroup"
            packages="curl tar gnupg2 openvpn openssl ca-certificates iptables fail2ban"

            printf "%s\n" "[INFO] detected Debian ${VERSION_ID}"
            test "${VERSION_ID}" -lt 9 && printf "%s\n" "[ERR] unsupported Debian under version 9, exiting" && exit 1
            printf "%s\n" "[INFO] installing packages: ${packages}"

            apt-get update -yqq >/dev/null
            apt-get install -yqq $packages >/dev/null
            ;;
        "centos")
            group_name="nobody"
            packages="curl tar openvpn openssl ca-certificates tar "

            printf "%s\n" "[INFO] detected CentOS ${VERSION_ID}"
            test "${VERSION_ID}" -lt 7 && printf "%s\n" "[ERR] unsupported CentOS under version 7, exiting" && exit 1
            test "${VERSION_ID}" -eq 7 && packages+="policycoreutils-python" || packages=+"policycoreutils-python-utils"
            printf "%s\n" "[INFO] installing packages: ${packages}"

            yum install -y epel-release >/dev/null
            yum install -y $packages >/dev/null
            ;;
        "fedora")
            group_name="nobody"
            packages="curl tar openvpn openssl ca-certificates tar"

            printf "%s\n" "[INFO] detected Fedora ${VERSION_ID}"
            printf "%s\n" "[INFO] installing packages: ${packages}"

            dnf install -y $packages >/dev/null
            ;;
	esac

    if command -v systemd-detect-virt &>/dev/null; then
        if systemd-detect-virt -cq; then
            mkdir -p /etc/systemd/system/openvpn-server@server.service.d/
            printf "[Service]\nLimitNPROC=infinity\n" > /etc/systemd/system/openvpn-server@server.service.d/disable-limitnproc.conf
        fi
    fi
}

function setup_firewall() {
    printf "%s\n" "[INFO] setting iptables rules and systemd network unit file"
    mkdir -p /etc/iptables &>/dev/null
    default_iface=$(ip -4 route | grep default | grep -Po '(?<=dev )(\S+)' | head -1)

    cat > /etc/iptables/add-openvpn-rules.sh << EOF
#!/bin/bash
iptables -t nat -I POSTROUTING 1 -s 10.8.0.0/24 -o ${default_iface} -j MASQUERADE
iptables -I INPUT 1 -i tun0 -j ACCEPT
iptables -I FORWARD 1 -i ${default_iface} -o tun0 -j ACCEPT
iptables -I FORWARD 1 -i tun0 -o ${default_iface} -j ACCEPT
iptables -I INPUT 1 -i ${default_iface} -p ${protocol} --dport ${port} -j ACCEPT
EOF

    cat > /etc/iptables/rm-openvpn-rules.sh << EOF
#!/bin/bash
iptables -t nat -D POSTROUTING -s 10.8.0.0/24 -o ${default_iface} -j MASQUERADE
iptables -D INPUT -i tun0 -j ACCEPT
iptables -D FORWARD -i ${default_iface} -o tun0 -j ACCEPT
iptables -D FORWARD -i tun0 -o ${default_iface} -j ACCEPT
iptables -D INPUT -i ${default_iface} -p ${protocol} --dport ${port} -j ACCEPT
EOF

    cat > /etc/systemd/system/iptables-openvpn.service << EOF
[Unit]
Description=iptables rules for OpenVPN
Before=network-online.target
Wants=network-online.target

[Service]
Type=oneshot
ExecStart=/etc/iptables/add-openvpn-rules.sh
ExecStop=/etc/iptables/rm-openvpn-rules.sh
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
EOF

    chmod +x /etc/iptables/add-openvpn-rules.sh
    chmod +x /etc/iptables/rm-openvpn-rules.sh

    if command -v systemctl &>/dev/null; then
        systemctl enable --now -q iptables-openvpn.service
        systemctl enable --now -q openvpn-server@server.service
        printf "%s\n" "[INFO] enabled and started iptables-openvpn.service and openpn-server@server.service"
    else
        printf "%s\n" "[WARN] \"systemctl\" not found, could not start iptables-openvpn.service and openpn-server@server.service"
    fi
}

function setup_fail2ban() {
    mkdir -p /etc/fail2ban/jail.d &>/dev/null
    mkdir -p /etc/fail2ban/filter.d &>/dev/null
    rm -rf /etc/fail2ban/jail.d/*

    cat > /etc/fail2ban/filter.d/openvpn.conf << EOF
# Fail2Ban filter for selected OpenVPN rejections

[INCLUDES]

# Read common prefixes. If any customizations available -- read them from
# common.local
before = common.conf

[Definition]
failregex = ^%(__prefix_line)sTLS Error: incoming packet authentication failed from \[AF_INET\]<HOST>:\d+$
            ^%(__prefix_line)s<HOST>:\d+ Connection reset, restarting
            ^%(__prefix_line)s<HOST>:\d+ TLS Auth Error
            ^%(__prefix_line)s<HOST>:\d+ TLS Error: TLS handshake failed$
            ^%(__prefix_line)s<HOST>:\d+ VERIFY ERROR
ignoreregex =
datepattern = ^%%b %%d %%H:%%M:%%S
EOF

    cat > /etc/fail2ban/jail.d/openvpn.conf << EOF
[openvpn]
enabled   = true
port      = ${port}
protocol  = ${protocol}
filter    = openvpn
backend   = systemd
#journalmatch = _SYSTEMD_UNIT=openvpn-server@server.service
maxretry  = 3
EOF

    cat > /etc/openvpn/server/fail2ban-openvpn.sh << "EOF"
#!/bin/bash
# -*- coding: utf-8 -*-

help() {
    printf "%s\n" "usage: $(basename $0) -s (status) | -b <ip> (banip) | -u <ip> (unbanip) | -r (reload) | -x (restart)"
    exit 1
}

test -z "$@" && help

while getopts "sb:u:rxh" ARG; do
    case "${ARG}" in
        s) fail2ban-client status; fail2ban-client status openvpn;;
        b) fail2ban-client set openvpn banip "${OPTARG}";;
        u) fail2ban-client set openvpn unbanip "${OPTARG}";;
        r) fail2ban-client reload;;
        x) fail2ban-client restart;;
        h) help;;
        *) help;;
    esac
done

EOF
    chmod +x /etc/openvpn/server/fail2ban-openvpn.sh
    if command -v systemctl &>/dev/null; then
        systemctl enable --now -q fail2ban
        systemctl reload -q fail2ban
        printf "%s\n" "[INFO] enabled and started fail2ban"
    else
        printf "%s\n" "[WARN] \"systemctl\" not found, could not enable and reload fail2ban"
    fi
}

if ! test -e "${OPENVPN_SERVER_ROOT}/server.conf"; then
    mkdir -p /etc/openvpn/clients >/dev/null

    port="31337"
    protocol="udp"
    dns="google"
    domain=""

    while getopts "s:p:r:d:o:h" ARG; do
        case "${ARG}" in
            s) server="${OPTARG}";;
            p) port="${OPTARG}";;
            r) protocol="${OPTARG}";;
            d) dns="${OPTARG}";;
            o) domain="${OPTARG}";;
            h) install_usage;;
            *) install_usage;;
        esac
    done

    test -z "${server}" && printf "%s\n" "[ERR] -s is mandatory" && install_usage

    if expr "${server}" : '^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$' >/dev/null; then
        external="${server}"
        printf "%s\n" "[INFO] using IP ${server}"
    else
        host_ip=$(host $server | grep -Eo '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')
        test $? -ne 0 && printf "%s\n" "[ERR] could not resolve ${server}" && exit 1
        test $(printf "%s\n" "${host_ip}" | wc -l) -gt 1 && printf "%s\n" "${server} has multiple ip addresses, exiting" && exit 1
        external="${server}"
        printf "%s\n" "[INFO] ${server} has ip ${host_ip}"
    fi

    expr "${port}" : '[0-9]\+$' >/dev/null && test "${port}" -ge 65535 && printf "%s\n" "[ERR] invalid port selection, exiting" && exit 1
    printf "%s\n" "[INFO] using port ${port}"
    printf "%s\n" "[INFO] using protocol ${protocol}"

    if test ! -z "${dns}"; then
        dns_opt="${dns_opt,,}"
        case "${dns_opt}" in
            "system")
                printf "%s\n" "[INFO] using system DNS resolvers"
                gw=$(ip route | awk '/default/ {print $3}')
                if test "${domain}" = ""; then
                    printf "%s\n" "[ERR] for dns type \"system\", supply -o for internal domain"
                    exit 1
                fi
                server_dns_servers="push \"dhcp-option DNS ${gw}\""$'\n'"push \"dhcp-option DOMAIN-ROUTE .\""$'\n'"push \"dhcp-option DOMAIN ${domain}\""
                client_dns_servers="dhcp-option DNS ${gw}"$'\n'"dhcp-option DOMAIN-ROUTE ."
                ;;

            "google")
                printf "%s\n" "[INFO] using Google DNS resolvers"
                server_dns_servers="push \"dhcp-option DNS 8.8.8.8\""$'\n'"push \"dhcp-option DNS 8.8.4.4\""$'\n'"push \"dhcp-option DOMAIN-ROUTE .\""
                client_dns_servers="dhcp-option DNS 8.8.8.8"$'\n'"dhcp-option DNS 8.8.4.4"$'\n'"dhcp-option DOMAIN-ROUTE ."
                ;;

            "cloudflare")
                printf "%s\n" "[INFO] using Cloudflare DNS resolvers"
                server_dns_servers="push \"dhcp-option DNS 1.1.1.1\""$'\n'"push \"dhcp-option DNS 1.0.0.1\""$'\n'"push \"dhcp-option DOMAIN-ROUTE .\""
                client_dns_servers="dhcp-option DNS 1.1.1.1"$'\n'"dhcp-option DNS 1.0.0.1"$'\n'"dhcp-option DOMAIN-ROUTE ."
                ;;

            "opendns")
                printf "%s\n" "[INFO] using OpenDNS DNS resolvers"
                server_dns_servers="push \"dhcp-option DNS 208.67.222.222\""$'\n'"push \"dhcp-option DNS 208.67.220.220\""$'\n'"push \"dhcp-option DOMAIN-ROUTE .\""
                client_dns_servers="dhcp-option DNS 208.67.222.222"$'\n'"dhcp-option DNS 208.67.220.220"$'\n'"dhcp-option DOMAIN-ROUTE ."
                ;;
        esac
    else
        server_dns_servers="push \"dhcp-option DNS 8.8.8.8\""$'\n'"push \"dhcp-option DNS 8.8.4.4\""$'\n'"push \"dhcp-option DOMAIN-ROUTE .\""
        client_dns_servers="dhcp-option DNS 8.8.8.8"$'\n'"dhcp-option DNS 8.8.4.4"$'\n'"dhcp-option DOMAIN-ROUTE ."
        printf "%s\n" "[INFO] using default Google DNS"
    fi

    setup

    if ! test -f "${OPENVPN_SERVER_ROOT}/server.key"; then
        mkdir -p "${OPENVPN_EASYRSA_ROOT}"
        curl -sSkL "${EASYRSA_RELEASE}" | tar zxf - --strip 1 -C "${OPENVPN_EASYRSA_ROOT}"
        chown -R root:root "${OPENVPN_EASYRSA_ROOT}"
        cd "${OPENVPN_EASYRSA_ROOT}"
        date -u >>"${OPENVPN_EASYRSA_ROOT}/install.log"
        ./easyrsa init-pki 2>>"${OPENVPN_EASYRSA_ROOT}/install.log" 1>&2
        ./easyrsa --batch build-ca nopass 2>>"${OPENVPN_EASYRSA_ROOT}/install.log" 1>&2
        ./easyrsa --batch --days=3650 build-server-full "${server}" nopass 2>>"${OPENVPN_EASYRSA_ROOT}/install.log" 1>&2
        ./easyrsa --batch --days=3650 gen-crl 2>>"${OPENVPN_EASYRSA_ROOT}/install.log" 1>&2
        cp pki/ca.crt pki/crl.pem pki/private/ca.key "pki/issued/${server}.crt" "pki/private/${server}.key" "${OPENVPN_SERVER_ROOT}"
        chown "nobody:${group_name}" "${OPENVPN_SERVER_ROOT}/crl.pem"
        chmod o+x "${OPENVPN_SERVER_ROOT}"
        openvpn --genkey secret "${OPENVPN_SERVER_ROOT}/tc.key"
    fi

    cat > "${OPENVPN_SERVER_ROOT}/dh.pem" << EOF
-----BEGIN DH PARAMETERS-----
MIIBCAKCAQEA//////////+t+FRYortKmq/cViAnPTzx2LnFg84tNpWp4TZBFGQz
+8yTnc4kmz75fS/jY2MMddj2gbICrsRhetPfHtXV/WVhJDP1H18GbtCFY2VVPe0a
87VXE15/V8k1mE8McODmi3fipona8+/och3xWKE2rec1MKzKT0g6eXq8CrGCsyT7
YdEIqUuyyOP7uWrat2DX9GgdT0Kj3jlN9K5W7edjcrsZCwenyO4KbXCeAvzhzffi
7MA0BM0oNC9hkXL+nOmFg/+OTxIy7vKBg8P+OxtMb61zO7X8vC7CIAXFjvGDfRaD
ssbzSibBsu/6iGtCOGEoXJf//////////wIBAg==
-----END DH PARAMETERS-----
EOF

    cat > "${OPENVPN_SERVER_ROOT}/server.conf" << EOF
# Specify the port and transport protocol
port ${port}
proto ${protocol}

# Specify the virtual tunnel device type
dev tun

# Certificate and key files
ca ${OPENVPN_SERVER_ROOT}/ca.crt
cert ${OPENVPN_SERVER_ROOT}/${server}.crt
key ${OPENVPN_SERVER_ROOT}/${server}.key
crl-verify ${OPENVPN_SERVER_ROOT}/crl.pem

# TLS configuration
tls-crypt ${OPENVPN_SERVER_ROOT}/tc.key
dh ${OPENVPN_SERVER_ROOT}/dh.pem
auth SHA512
tls-version-min 1.2
tls-server
tls-cipher TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256:TLS-ECDHE-ECDSA-WITH-AES-128-GCM-SHA256:TLS-ECDHE-RSA-WITH-AES-256-GCM-SHA384:TLS-DHE-RSA-WITH-AES-256-CBC-SHA256

# Network configuration
topology subnet
server 10.8.0.0 255.255.255.0

# Buffer settings
sndbuf 0
rcvbuf 0

# IP address persistence
ifconfig-pool-persist ipp.txt

# Push route to clients
push "redirect-gateway def1 bypass-dhcp"
${server_dns_servers}

# Keepalive settings
keepalive 10 120

# Encryption settings
cipher AES-256-CBC

# User and group settings
user nobody
group ${group_name}

# Persistence settings
persist-key
persist-tun

# Verbosity level
# Change to 0 to disable logging
verb 3

# Optional directives for logging and status output
#log /dev/null
#status /dev/null

# Uncomment to enable slack notifications on connect/disconnect
#script-security 2
#client-connect /etc/openvpn/server/slack_notification.sh
#client-disconnect /etc/openvpn/server/slack_notification.sh
EOF
    test "${protocol}" = "udp" && printf "%s\n" "explicit-exit-notify" >> "${OPENVPN_SERVER_ROOT}/server.conf"

    printf "%s\n" "net.ipv4.ip_forward=1" > /etc/sysctl.d/30-openvpn-forward.conf
    if test -w /proc/sys/net/ipv4/ip_forward; then
        printf "%s\n" "1" > /proc/sys/net/ipv4/ip_forward
    else
        printf "%s\n" "[WARN] \"/proc/sys/net/ipv4/ip_forward\" is not writable"
    fi

    cat > "${OPENVPN_SERVER_ROOT}/client-common.txt" << EOF
# Client configuration
client

# Specify the virtual tunnel device type
dev tun

# Specify the transport protocol and server
proto ${protocol}
remote ${external} ${port}

# Retry indefinitely if connection fails
resolv-retry infinite

# Do not bind to a specific local port
nobind

# Persist key and tunnel device across restarts
persist-key
persist-tun

# Require server certificate verification
remote-cert-tls server

# TLS configuration
tls-version-min 1.2
tls-client
tls-cipher TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256:TLS-ECDHE-ECDSA-WITH-AES-128-GCM-SHA256:TLS-ECDHE-RSA-WITH-AES-256-GCM-SHA384:TLS-DHE-RSA-WITH-AES-256-CBC-SHA256

# Authentication and encryption settings
auth SHA512
cipher AES-256-CBC

# Buffer settings
sndbuf 0
rcvbuf 0

# Verbosity level for logging
verb 3
EOF

    if test "${dns_opt}" = "system"; then
        cat >> "${OPENVPN_SERVER_ROOT}/client-common.txt" << EOF
# Install resolvconf or openvpn-systemd-resolved
script-security 2
up /etc/openvpn/update-systemd-resolved # or update-resolv-conf
up-restart
down /etc/openvpn/update-systemd-resolved # or update-resolv-conf
down-pre

# The following options might be incompatible with OpenVPN GUI v2; v3 should be fine
${client_dns_servers}
EOF
    fi

    setup_firewall
    setup_fail2ban
    setup_slack_notification
    client_usage

else
    while getopts "a:r:luh" ARG; do
        case "${ARG}" in
            a) 
                action="add"
                name="${OPTARG}"
                ;;
            r) 
                action="revoke"
                name="${OPTARG}"
                ;;
            l) action="list";;
            u) action="uninstall";;
            *) client_usage;;
        esac
    done

    if ! test "${action}" = "add" && ! test "${action}" = "revoke" && ! test "${action}" = "list" && ! test "${action}" = "uninstall"; then
        client_usage
    fi

    case "${action}" in
        "add")
            client="${name}"
            if test -e "${OPENVPN_EASYRSA_ROOT}/pki/issued/${client}.crt"; then
                printf "%s\n" "[ERR] client name \"${client}\" already exists, choose another"
                exit 1
            fi

            cd "${OPENVPN_EASYRSA_ROOT}"
            date -u >>"${OPENVPN_EASYRSA_ROOT}/install.log"
            ./easyrsa --batch --days=3650 build-client-full "${client}" nopass 2>>"${OPENVPN_EASYRSA_ROOT}/install.log" 1>&2
            new_client
            ;;
        "revoke")
            client="${name}"
            if ! test -e "${OPENVPN_EASYRSA_ROOT}/pki/issued/${client}.crt"; then
                printf "%s\n" "[ERR] client name \"${client}\" for revocation doesn't exist, exiting"
                exit 1
            fi

            cd "${OPENVPN_EASYRSA_ROOT}"
            date -u >>"${OPENVPN_EASYRSA_ROOT}/install.log"
            ./easyrsa --batch revoke "${client}" 2>>"${OPENVPN_EASYRSA_ROOT}/install.log" 1>&2
            ./easyrsa --batch --days=3650 gen-crl 2>>"${OPENVPN_EASYRSA_ROOT}/install.log" 1>&2
            rm -f "${OPENVPN_SERVER_ROOT}/crl.pem"
            cp "${OPENVPN_EASYRSA_ROOT}/pki/crl.pem" "${OPENVPN_SERVER_ROOT}/crl.pem"
            chown "nobody:${group_name}" "${OPENVPN_SERVER_ROOT}/crl.pem"
            printf "%s\n" "[INFO] revoked client \"${client}\""
            ;;
        "list")
            valid_clients=$(cat "${OPENVPN_EASYRSA_ROOT}/pki/index.txt" | grep -Eo '^V.*' | awk -F '/CN=' '{print $2}' | sed ':a;N;$!ba;s/\n/, /g')
            revoked_clients=$(cat "${OPENVPN_EASYRSA_ROOT}/pki/index.txt" | grep -Eo '^R.*' | awk -F '/CN=' '{print $2}' | sed ':a;N;$!ba;s/\n/, /g')

            if ! test -z "${valid_clients}"; then
                printf "%s\n" "[INFO] valid client certificates: ${valid_clients}"
            fi

            if ! test -z "${revoked_clients}"; then
                printf "%s\n" "[INFO] revoked client certificates: ${revoked_clients}"
            fi
            ;;
        "uninstall")
            if command -v systemctl &>/dev/null; then
                systemctl disable --now -q iptables-openvpn.service
                systemctl disable --now -q openvpn-server@server.service
                printf "%s\n" "[INFO] stopped services"
            else
                printf "%s\n" "[WARN] \"systemctl\" not found, could not disable iptables-openvpn.service and openvpn-server@sever.service"
            fi

            rm -rf /etc/openvpn /etc/systemd/system/iptables-openvpn.service
            if command -v iptables &>/dev/null; then
                iptables -F
            fi
            printf "%s\n" "[INFO] removed openvpn installation, certificates and iptables service files"
            ;;
    esac
fi
