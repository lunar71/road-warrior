#!/bin/bash

test $EUID -ne 0 && printf "%s\n" "run as root" && exit 1

if ! command -v systemctl &>/dev/null; then
    printf "%s\n" "[ERR] distro is not using systemd"
    exit 1
fi

if ! command -v apt-get &>/dev/null; then
    printf "%s\n" "[ERR] distro is not debian-based"
    exit 1
fi

if ! command -v tigervncserver &>/dev/null; then
    DEBIAN_FRONTEND=noninteractive apt-get update
    DEBIAN_FRONTEND=noninteractive apt-get install -y tigervnc-standalone-server
fi

cat > /etc/systemd/system/vncserver@.service << EOF
[Unit]
Description=Remote desktop service (VNC)
After=syslog.target network.target

[Service]
Type=forking
User=%i
Group=%i
WorkingDirectory=/home/%i
Environment=HOME=/home/%i
Environment=USER=%i
ExecStart=vncserver -localhost -securitytypes none -depth 24
ExecStop=vncserver -kill :*

[Install]
WantedBy=graphical.target
EOF

printf "%s\n" \
    "[INFO] installed and configured tigervnc systemd service" \
    "[INFO] enable and start the vnc server systemd service for a user with \"vncserver@<user>.service\"" \
    "[INFO] the vnc server will start running on localhost on the first available port e.g., 5901 for <user>" \
    "[INFO] connect to the remote vnc server with:" \
    "  - vncviewer localhost:1 -via <user>@<server>" \
    "  or" \
    "  - ssh <user>@<server> -fNL 5901:localhost:5901; vncviewer localhost:5901"
