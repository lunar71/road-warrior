#!/bin/bash
# -*- coding: utf-8 -*-
# references for multi-peer chain: https://serverfault.com/questions/1088689/wireguard-use-one-client-as-gateway-of-another
set -e 

SCRIPTPATH=$(realpath "${0}")
SCRIPTNAME=$(basename "${SCRIPTPATH}")
WG_SERVER_HOME="/etc/wireguard"
WG_PEERS_HOME="${WG_SERVER_HOME}/peers.d"
WG_NODES_HOME="${WG_SERVER_HOME}/nodes.d"
WG_CHAINS_HOME="${WG_SERVER_HOME}/chains.d"
IP_FILE="${WG_SERVER_HOME}/ips.txt"

test "${EUID}" -ne 0 && printf "%s\n" "run as root" && exit 1
umask 077

if ! command -v apt-get &>/dev/null; then
    printf "%s\n" "[ERR] not debian-based distro"
    exit 1
fi

function usage() {
    printf "%s\n" \
        "" \
        "wireguard deployment script with optional internal lan routing configs" \
        "usage: ${SCRIPTNAME} <action> [-h] <options>" \
        "" \
        "install-server     install and configure the wg server" \
        "uninstall-server   uninstall the wg server" \
        "add-peer           add a peer to the wg network" \
        "add-node           add a node to the wg network" \
        "add-chain          add a chain to the wg network" \
        "remove-peer        remove peer from the wg network" \
        "remove-node        remove node from the wg network" \
        "remove-chain       remove chain from the wg network" \
        "list               list peers or nodes" \
        "status             list current wg status" \
        "usage              print this help message and exit" \
        "extusage           print extended help message and exit" \
        "" \
        "configuration files:" \
        "wg server config dir: ${WG_SERVER_HOME}" \
        "wg peers configs dir: ${WG_PEERS_HOME}" \
        "wg nodes configs dir: ${WG_NODES_HOME}" \
        "wg chain configs dir: ${WG_CHAINS_HOME}"

    exit 1
}

function extended_usage() {
    printf "%s\n" \
        "" \
        "wireguard deployment script with optional internal lan routing configs" \
        "usage: ${SCRIPTNAME} <action> [-h] <options>" \
        "" \
        "install-server     install and configure the wg server" \
        "options:" \
        "   -p <port>       optional: port to use, default 31337" \
        "   -i <interface>  optional: interface to use, default auto-detected" \
        "" \
        "uninstall-server   uninstall the wg server" \
        "" \
        "add-peer           add a peer to the wg network" \
        "options:" \
        "    -s <server>    required: wg server endpoint" \
        "    -n <name>      optional: peer name" \
        "    -p <port>      optional: wg server port, default 31337" \
        "    -d <dns>       optional: dns server to use e.g., internal lan dns server; default 8.8.8.8" \
        "" \
        "topology:" \
        "peer <------> server A <------> internet" \
        "A peer        main server" \
        "10.0.0.2      10.0.0.1" \
        "" \
        "add-node           add a node to the wg network" \
        "options:" \
        "    -s <server>    required: wg server endpoint" \
        "    -n <name>      optional: peer name" \
        "    -p <port>      optional: wg server port, default 31337" \
        "    -i <interface> required: interface to use for node only" \
        "    -l <cidr>      optional: subnet cidr to set for internal lan access" \
        "" \
        "topology:" \
        "peer <------> server A <------> internet <------> lan" \
        "A peer        main server                         A peer" \
        "10.0.0.3      10.0.0.1                            10.0.0.2, 192.168.1.0/24" \
        "" \
        "add-chain          add a chain to the wg network" \
        "options:" \
        "    -s <server>    required: wg server endpoint" \
        "    -n <name>      optional: peer name" \
        "    -p <port>      optional: wg server port, default 31337" \
        "" \
        "topology:" \
        "peer <------> server A <------> server B <------> internet" \
        "A peer        chain server      main server" \
        "10.0.0.3      10.0.0.2          10.0.0.1" \
        "" \
        "remove-peer        remove peer from the wg network" \
        "options:" \
        "    -n <name>      remove a peer by name from wg0.conf" \
        "" \
        "remove-node        remove node from the wg network" \
        "options:" \
        "    -n <name>      remove a node by name from wg0.conf" \
        "" \
        "remove-chain       remove chain from the wg network" \
        "options:" \
        "    -n <name>      remove a chain by name from wg0.conf" \
        "" \
        "list               list peers or nodes" \
        "status             list current wg status" \
        "usage              print this help message and exit" \
        "extusage           print extended help message and exit" \
        "" \
        "configuration files:" \
        "wg server config dir: ${WG_SERVER_HOME}" \
        "wg peers configs dir: ${WG_PEERS_HOME}" \
        "wg nodes configs dir: ${WG_NODES_HOME}" \
        "wg chain configs dir: ${WG_CHAINS_HOME}" \
        "" \
        "examples" \
        "" \
        "install wg server with simple peers" \
        "- install wg server:" \
        "    ./${SCRIPTNAME} install-server" \
        "- add simple peer:" \
        "    ./${SCRIPTNAME} add-peer -s 12.13.14.15 -n peer01 -p 31337" \
        "- add simple peer with dns settings:" \
        "    ./${SCRIPTNAME} add-peer -s 12.13.14.15 -n peer01 -p 31337 -d 1.1.1.1" \
        "" \
        "install wg server with lan node and lan access peers" \
        "- install wg server:" \
        "    ./${SCRIPTNAME} install-server" \
        "- add node for internal lan access:" \
        "    ./${SCRIPTNAME} add-node -s 12.13.14.15 -n node01 -p 31337 -i ens18 -l 192.168.1.0/24" \
        "- add peer with lan dns settings:" \
        "    ./${SCRIPTNAME} add-peer -s 12.13.14.15 -n peer01 -p 31337 -d 192.168.1.1" \
        "" \
        "install wg server with chain and peers" \
        "- install wg server:" \
        "    ./${SCRIPTNAME} install-server" \
        "- add chain for downstream server to upstream server:" \
        "    ./${SCRIPTNAME} add-chain -s 12.13.14.15 -n chain01 -p 31337" \
        "- run the generated deployment script on chaining server" \
        "- transfer ${WG_CHAINS_HOME}/chain01/chain01_deploy.sh to chaining server" \
        "    ./chain01_deploy.sh" \
        "- add peer from the chained server" \
        "    ./${SCRIPTNAME} add-peer -s 12.13.14.15 -n peer01 -p 31337" \
        "" \
        "list generated peers, nodes and chains" \
        "./${SCRIPTNAME} list" \
        "" \
        "remove a peer" \
        "./${SCRIPTNAME} remove-peer -n peer01" \
        "" \
        "remove a node" \
        "./${SCRIPTNAME} remove-node -n node01" \
        "" \
        "remove a chain" \
        "./${SCRIPTNAME} remove-node -n chain01" \
        "" \
        "get current wg0 status" \
        "./${SCRIPTNAME} status"

    exit 1
}

function install_usage() {
    printf "%s\n" \
        "install wireguard server" \
        "usage: ${SCRIPTNAME} install-server -p <port> -i <interface>" \
        "" \
        "-p <port>         optional: port to use, default 31337" \
        "-i <interface>    optional: interface to use, default auto-detected"

    exit 1
}

function uninstall_usage() {
    printf "%s\n" \
        "uninstall the wireguard server" \
        "usage: ${SCRIPTNAME} uninstall-server" \
        "" \
        "uninstall the wg server"

    exit 1
}

function addpeer_usage() {
    printf "%s\n" \
        "add a wireguard peer" \
        "usage: ${SCRIPTNAME} add-peer -s <server> [-n <name>] [-p <port>] [-d <dns>]" \
        "" \
        "-s <server>       required: wg server endpoint" \
        "-n <name>         optional: peer name" \
        "-p <port>         optional: wg server port, default 31337" \
        "-d <dns>          optional: dns server to use e.g., internal lan dns server; default 8.8.8.8"

    exit 1
}

function addnode_usage() {
    printf "%s\n" \
        "add a wireguard node to expose an internal lan segment" \
        "usage: ${SCRIPTNAME} add-node -s <server> -i <interface> [-n <name>] [-p <port>] -l <cidr>" \
        "" \
        "-s <server>       required: wg server endpoint" \
        "-i <interface>    required: interface to use for node only" \
        "-n <name>         optional: peer name" \
        "-p <port>         optional: wg server port, default 31337" \
        "-l <cidr>         optional: subnet cidr to set for internal lan access"

    exit 1
}

function addchain_usage() {
    printf "%s\n" \
        "add a wireguard chain from a downstream server to an upstream server" \
        "usage: ${SCRIPTNAME} add-chain -s <server> [-n <name>] [-p <port>]" \
        "" \
        "-s <server>       required: wg server endpoint" \
        "-n <name>         optional: peer name" \
        "-p <port>         optional: wg server port, default 31337"

    exit 1
}

function removepeer_usage() {
    printf "%s\n" \
        "remove a wireguard peer" \
        "usage: ${SCRIPTNAME} remove-peer -n <name>" \
        "" \
        "remove a peer by name from wg0.conf"

    exit 1
}

function removenode_usage() {
    printf "%s\n" \
        "remove a wireguard node" \
        "usage: ${SCRIPTNAME} remove-node -n <name>" \
        "" \
        "remove a node by name from wg0.conf"

    exit 1
}

function removechain_usage() {
    printf "%s\n" \
        "remove a wireguard chain" \
        "usage: ${SCRIPTNAME} remove-chain -n <name>" \
        "" \
        "remove a node by name from wg0.conf"

    exit 1
}

function check_deps() {
    deps="sysctl systemctl ip"
    for i in $deps; do
        if ! command -v "${i}" &>/dev/null; then
            printf "%s\n" "[ERR] ${i} not found"
            exit 1
        fi
    done
}

function install_wg() {
    if ! command -v wg &>/dev/null; then
        export DEBIAN_FRONTEND="noninteractive"
        apt-get update -yq
        apt-get install -yq wireguard wireguard-tools iptables qrencode
        apt-get clean
    fi
}

function get_next_available_ip() {
    (
        flock -x 200
        touch "${IP_FILE}"
        for i in {2..254}; do
            ip="10.0.0.${i}"
            if ! grep -q "${ip}" "${IP_FILE}"; then
                printf "%s\n" "${ip}"
                printf "%s\n" "${ip}" >> "${IP_FILE}"
                exit 0
            fi
        done
        printf "%s\n" "[ERR] no available ips in range 10.0.0.2 - 10.0.0.254"
        exit 1
    ) 200>"${IP_FILE}.lock"
}

function get_default_iface() {
    if command -v ip &>/dev/null; then
        default_iface=$(ip r | awk '/default/ {print $5}')
        if test -z "${default_iface}"; then
            printf "%s\n" "[ERR] could not identify default route interface"
            exit 1
        fi
        printf "%s\n" "${default_iface}"
    else
        printf "%s\n" "[ERR] iproute2 not found"
        exit 1
    fi
}

function validate_ip() {
    local ip="${1}"
    if ! [[ "${ip}" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
        printf "%s\n" "[ERR] invalid IP address: ${ip}"
        exit 1
    fi
}

function validate_port() {
    local port="${1}"
    if ! [[ "${port}" =~ ^[0-9]+$ ]] || [ "${port}" -lt 1 ] || [ "${port}" -gt 65535 ]; then
        printf "%s\n" "[ERR] invalid port: ${port}"
        exit 1
    fi
}

function validate_cidr() {
    local cidr="${1}"
    if ! [[ "${cidr}" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/[0-9]+$ ]] || \
       ! [[ "$(printf "%s\n" "${cidr}" | cut -d'/' -f2)" -le 32 ]]; then
        printf "%s\n" "[ERR] invalid CIDR: ${cidr}"
        exit 1
    fi
}

function install_wg_server() {
    install_wg

    local port="$1"
    local interface="$2"
    port="${port:-31337}"
    interface="${interface:-$(get_default_iface)}"

    if ! test "${interface}"; then
        interface="$(get_default_iface)"
    fi

    if test -d "${WG_SERVER_HOME}" && test -f "${WG_SERVER_HOME}/wg0.conf"; then
        printf "%s\n" "[ERR] ${WG_SERVER_HOME}/wg0.conf already exists, remove existing server and rerun"
        exit 1
    fi

    printf "%s\n" \
        "[INFO] installing wg server on port ${port}" \
        "[INFO] using interface ${interface}" 

    mkdir -p "${WG_SERVER_HOME}" &>/dev/null
    (
        cd "${WG_SERVER_HOME}"
        wg genpsk > psk.key
        wg genkey > server.key
        cat > wg0.conf << EOF
# main wg server
[Interface]
PrivateKey = $(cat server.key)
Address    = 10.0.0.1/24
ListenPort = ${port}
PostUp     = sysctl -w net.ipv4.ip_forward=1
PostUp     = iptables -A FORWARD -i ${interface} -o %i -j ACCEPT
PostUp     = iptables -A FORWARD -i %i -j ACCEPT
PostUp     = iptables -t nat -A POSTROUTING -o ${interface} -j MASQUERADE
PostDown   = sysctl -w net.ipv4.ip_forward=0
PostDown   = iptables -D FORWARD -i ${interface} -o %i -j ACCEPT
PostDown   = iptables -D FORWARD -i %i -j ACCEPT
PostDown   = iptables -t nat -D POSTROUTING -o ${interface} -j MASQUERADE
EOF
        printf "%s\n" "[INFO] created ${WG_SERVER_HOME}/server.key and ${WG_SERVER_HOME}/wg0.conf"
        if systemctl enable -q --now wg-quick@wg0.service; then
            printf "%s\n" "[INFO] enabled and started wg-quick@wg0.service"
        else
            printf "%s\n" "[ERR] failed to enable and start wg-quick@wg0.service"
            exit 1
        fi
    )
}

function uninstall_wg_server() {
    if systemctl is-active wg-quick@wg0.service &>/dev/null; then
        systemctl disable -q --now wg-quick@wg0.service
        sleep 3
        printf "%s\n" "[INFO] found and disabled wg-quick@wg0.service"
    fi

    rm -rf "${WG_SERVER_HOME}"
    printf "%s\n" "[INFO] removed ${WG_SERVER_HOME}"
}

function add_peer() {
    if ! test -d "${WG_SERVER_HOME}" && ! test -f "${WG_SERVER_HOME}/wg0.conf"; then
        printf "%s\n" "[ERR] no wireguard server config found; install the server first"
        exit 1
    fi

    if ! test "${server}"; then
        printf "%s\n" "[ERR] missing -s <server>"
        exit 1
    fi

    mkdir -p "${WG_PEERS_HOME}/${name}" &>/dev/null
    assigned_ip=$(get_next_available_ip)
    (
        cd "${WG_PEERS_HOME}/${name}"
        wg genkey | tee "${rand}.key" | wg pubkey > "${rand}.pub"

        cat > "${rand}.conf" << EOF
# peer ${name}
[Interface]
PrivateKey = $(cat "${rand}.key")
Address    = ${assigned_ip}/24
DNS        = ${dns}

[Peer]
PublicKey           = $(cat "${WG_SERVER_HOME}/server.key" | wg pubkey)
PresharedKey        = $(cat "${WG_SERVER_HOME}/psk.key" )
Endpoint            = ${server}:${port}
AllowedIPs          = 0.0.0.0/0
PersistentKeepalive = 25
EOF
        printf "%s\n" \
            "[INFO] generated peer configuration ${name} to server ${server}" \
            "[INFO] config: ${WG_PEERS_HOME}/${name}/${rand}.conf" \
            "[INFO] any connectivity issues might be resolved by restarting wg-quick@wg0.service"

        qrencode -t ansiutf8 < "${WG_PEERS_HOME}/${name}/${rand}.conf" | tee "${WG_PEERS_HOME}/${name}/${name}_qr.ansi"
        qrencode -t png32 < "${WG_PEERS_HOME}/${name}/${rand}.conf" -o "${WG_PEERS_HOME}/${name}/${name}_qr.png"
        printf "%s\n" \
            "[INFO] saved qr codes as:" \
            "  - ${WG_PEERS_HOME}/${name}/${name}_qr.ansi" \
            "  - ${WG_PEERS_HOME}/${name}/${name}_qr.png"

        tar czf "${WG_PEERS_HOME}/${name}.tar.gz" -C "${WG_PEERS_HOME}/${name}" .
        printf "%s\n" "[INFO] compressed and archived ${name} peer config and qr codes to ${WG_PEERS_HOME}/${name}.tar.gz"
    )

    (
        peer_public_key=$(cat "${WG_PEERS_HOME}/${name}/${rand}.key" | wg pubkey) 
        cat >> "${WG_SERVER_HOME}/wg0.conf" << EOF

# peer ${name}
[Peer]
PublicKey    = ${peer_public_key}
PresharedKey = $(cat "${WG_SERVER_HOME}/psk.key")
AllowedIPs   = ${assigned_ip}/32
# peer ${name}
EOF
        if systemctl restart -q wg-quick@wg0.service; then
            printf "%s\n" "[INFO] restarted wg-quick@wg0.service"
        else
            printf "%s\n" "[ERR] failed to restart wg-quick@wg0.service"
            exit 1
        fi
    )
}

function add_node() {
    if ! test -d "${WG_SERVER_HOME}" && ! test -f "${WG_SERVER_HOME}/wg0.conf"; then
        printf "%s\n" "[ERR] no wireguard server config found; install the server first"
        exit 1
    fi

    if ! test "${server}" || ! test "${interface}" || ! test "${lan}"; then
        printf "%s\n" "[ERR] missing -s <server>, -i <interface> or -l <lan> arguments"
        exit 1
    fi

    mkdir -p "${WG_NODES_HOME}/${name}" &>/dev/null
    assigned_ip=$(get_next_available_ip)
    (
        cd "${WG_NODES_HOME}/${name}"
        wg genkey | tee "${rand}.key" | wg pubkey > "${rand}.pub"
            
        cat > "${rand}.conf" << EOF
# node ${name}
[Interface]
PrivateKey = $(cat "${rand}.key")
Address    = ${assigned_ip}/32
PostUp     = sysctl -w net.ipv4.ip_forward=1
PostUp     = iptables -A FORWARD -i %i -o ${interface} -j ACCEPT
PostUp     = iptables -A FORWARD -i ${interface} -o %i -j ACCEPT
PostUp     = iptables -t nat -A POSTROUTING -o ${interface} -j MASQUERADE
PostDown   = sysctl -w net.ipv4.ip_forward=0
PostDown   = iptables -D FORWARD -i %i -o ${interface} -j ACCEPT
PostDown   = iptables -D FORWARD -i ${interface} -o %i -j ACCEPT
PostDown   = iptables -t nat -D POSTROUTING -o ${interface} -j MASQUERADE

[Peer]
PublicKey           = $(cat "${WG_SERVER_HOME}/server.key" | wg pubkey)
PresharedKey        = $(cat "${WG_SERVER_HOME}/psk.key" )
Endpoint            = ${server}:${port}
AllowedIPs          = 0.0.0.0/0
PersistentKeepalive = 25
EOF
    )

    (
        cd "${WG_SERVER_HOME}"
        node_public_key=$(cat "${WG_NODES_HOME}/${name}/${rand}.key" | wg pubkey)
        cat >> wg0.conf << EOF

# node ${name}
[Peer]
PublicKey    = ${node_public_key}
PresharedKey = $(cat "${WG_SERVER_HOME}/psk.key" )
AllowedIPs   = ${assigned_ip}/32, ${lan}
# node ${name}
EOF
        if systemctl restart -q wg-quick@wg0.service; then
            printf "%s\n" "[INFO] restarted wg-quick@wg0.service"
        else
            printf "%s\n" "[ERR] failed to restart wg-quick@wg0.service"
            exit 1
        fi
    )
    printf "%s\n" \
        "[INFO] generated node configuration ${name} to server ${server}; routing enabled for internal network access" \
        "[INFO] config: ${WG_NODES_HOME}/${name}/${rand}.conf" \
        "[INFO] any connectivity issues might be resolved by restarting wg-quick@wg0.service"

    cat > "${WG_NODES_HOME}/${name}/${rand}_deploy.sh" << EOF
#!/bin/bash
# wireguard node "${name}" configuration and installation script
# connects to ${server}:${port} and exposes ${lan}

set -e

test \${EUID} -ne 0 && printf "%s\\n" "run as root" && exit 1
umask 077

if command -v apt-get &>/dev/null; then
    DEBIAN_FRONTEND=noninteractive apt-get update
    DEBIAN_FRONTEND=noninteractive apt-get install -yq wireguard wireguard-tools
else
    printf "%s\\n" "[ERR] distro not debian-based"
    exit 1
fi

mkdir -p /etc/wireguard &>/dev/null
cat > /etc/wireguard/${rand}.conf << EOL
$(cat "${WG_NODES_HOME}/${name}/${rand}.conf")
EOL

printf "%s\\n" \\
    "[INFO] created /etc/wireguard/${rand}.conf" \\
    "[INFO] enable and start the wg connection with systemctl enable --now wg-quick@${rand}.service"

exit 0
EOF
    printf "%s\n" "[INFO] generated auto-deployment script ${WG_NODES_HOME}/${name}/${rand}_deploy.sh for node ${name}"
}

function add_chain() {
    if ! test -d "${WG_SERVER_HOME}" && ! test -f "${WG_SERVER_HOME}/wg0.conf"; then
        printf "%s\n" "[ERR] no wireguard server config found; install the server first"
        exit 1
    fi

    if ! test "${server}"; then
        printf "%s\n" "[ERR] missing -s <server>"
        exit 1
    fi

    mkdir -p "${WG_CHAINS_HOME}/${name}" &>/dev/null
    assigned_ip=$(get_next_available_ip)
    (
        cd "${WG_CHAINS_HOME}/${name}"
        wg genkey | tee "${name}.key" | wg pubkey > "${name}.pub"

        cat > "${name}.conf" << EOF
# chain ${name}
[Interface]
PrivateKey  = $(cat "${name}.key")
Address     = ${assigned_ip}/24
ListenPort  = ${port}
Table       = 123
PostUp      = sysctl -w net.ipv4.ip_forward=1
PostUp      = ip rule add from 10.0.0.0/24 table 123
PreDown     = ip rule del from 10.0.0.0/24 table 123
PostDown    = sysctl -w net.ipv4.ip_forward=0

[Peer]
PublicKey           = $(cat "${WG_SERVER_HOME}/server.key" | wg pubkey)
PresharedKey        = $(cat "${WG_SERVER_HOME}/psk.key" )
Endpoint            = ${server}:${port}
AllowedIPs          = 0.0.0.0/0
PersistentKeepalive = 25
EOF
    )

    (
        peer_public_key=$(cat "${WG_CHAINS_HOME}/${name}/${name}.key" | wg pubkey) 
        cat >> "${WG_SERVER_HOME}/wg0.conf" << EOF

# chain ${name}
[Peer]
PublicKey    = ${peer_public_key}
PresharedKey = $(cat "${WG_SERVER_HOME}/psk.key")
AllowedIPs   = ${assigned_ip}/32
# peer ${name}
EOF
        if systemctl restart -q wg-quick@wg0.service; then
            printf "%s\n" "[INFO] restarted wg-quick@wg0.service"
        else
            printf "%s\n" "[ERR] failed to restart wg-quick@wg0.service"
            exit 1
        fi
    )

    cat > "${WG_CHAINS_HOME}/${name}/${name}_deploy.sh" << EOF
#!/bin/bash
# wireguard chain "${name}" configuration and installation script
# connects to ${server}:${port} to form a wg chain
set -e

test \${EUID} -ne 0 && printf "%s\\n" "run as root" && exit 1
umask 077

if ! command -v wg &>/dev/null; then
    printf "%s\\n" "[ERR] wireguard not installed"
    exit 1
fi

mkdir -p /etc/wireguard &>/dev/null

cat > "${IP_FILE}" << EOL
$(cat ${IP_FILE})
EOL

printf "%s" "$(cat ${WG_CHAINS_HOME}/${name}/${name}.key)" > /etc/wireguard/server.key
wg genpsk > "/etc/wireguard/psk.key"
cat > /etc/wireguard/wg0.conf << EOL
$(cat "${WG_CHAINS_HOME}/${name}/${name}.conf")
EOL

if command -v base64 &>/dev/null; then
    base64 -d <<< $(base64 -w0 "${SCRIPTPATH}") > "${HOME}/${SCRIPTNAME}"
    chmod +x "${HOME}/${SCRIPTNAME}"
    printf "%s\\n" "[INFO] copied ${SCRIPTNAME} to ${HOME}/${SCRIPTNAME}"
else
    printf "%s\\n" "[WARN] base64 not found; skipping ${SCRIPTNAME} copy"
fi

printf "%s\\n" \\
    "[INFO] created /etc/wireguard/wg0.conf as a downstream chain for ${server}:${port}" \\
    "[INFO] enable and start the wg connection with systemctl enable --now wg-quick@wg0.service" \
    "[INFO] use ${HOME}/${SCRIPTNAME} to add additional peers, nodes or chains"

exit 0
EOF
    printf "%s\n" \
        "[INFO] generated auto-deployment script ${WG_CHAINS_HOME}/${name}/${name}_deploy.sh for chain ${name}" \
        "[INFO] run this script on a downstream server to connect it to this wg server"
}

function remove_peer() {
    if test "${name}"; then
        if grep -qi "^# peer ${name}" "${WG_SERVER_HOME}/wg0.conf"; then
             sed -i "/# peer ${name}/,/# peer ${name}/ s/^/# /" "${WG_SERVER_HOME}/wg0.conf"
             systemctl restart -q wg-quick@wg0.service
             printf "%s\n" "[INFO] removed peer ${name} and restarted wg server"
         else
             printf "%s\n" "[ERR] no such peer ${name} in ${WG_SERVER_HOME}/wg0.conf"
             exit 1
        fi
    else
        removepeer_usage
    fi
}

function remove_node() {
    if test "${name}"; then
        if grep -qi "^# node ${name}" "${WG_SERVER_HOME}/wg0.conf"; then
            sed -i "/# node ${name}/,/# node ${name}/ s/^/# /" "${WG_SERVER_HOME}/wg0.conf"
            systemctl restart -q wg-quick@wg0.service
            printf "%s\n" "[INFO] removed node ${name} and restarted wg server"
         else
             printf "%s\n" "[ERR] no such node ${name} in ${WG_SERVER_HOME}/wg0.conf"
             exit 1
        fi
    else
        removenode_usage
    fi
}

function remove_chain() {
    if test "${name}"; then
        if grep -qi "^# chain ${name}" "${WG_SERVER_HOME}/wg0.conf"; then
            sed -i "/# chain ${name}/,/# chain ${name}/ s/^/# /" "${WG_SERVER_HOME}/wg0.conf"
            systemctl restart -q wg-quick@wg0.service
            printf "%s\n" "[INFO] removed chain ${name} and restarted wg server"
         else
             printf "%s\n" "[ERR] no such chain ${name} in ${WG_SERVER_HOME}/wg0.conf"
             exit 1
        fi
    else
        removechain_usage
    fi
}

function list() {
    test -d "${WG_PEERS_HOME}" && peers=$(find "${WG_PEERS_HOME}" -mindepth 1 -type d -exec basename {} \;) || peers=""
    test -d "${WG_NODES_HOME}" && nodes=$(find "${WG_NODES_HOME}" -mindepth 1 -type d -exec basename {} \;) || nodes=""
    test -d "${WG_CHAINS_HOME}" && chains=$(find "${WG_CHAINS_HOME}" -mindepth 1 -type d -exec basename {} \;) || nodes=""
    
    printf "%s\n" "[INFO] wg peers:"
    if test "${peers}"; then
        for i in ${peers}; do
            pubkey=$(find "${WG_PEERS_HOME}/${i}" -type f -iname *.pub -exec cat {} \;)
            if grep -qi "^#.*${pubkey}" "${WG_SERVER_HOME}/wg0.conf"; then
                printf -- "- %-16s %-16s (removed/disabled)\n" "${i}" "${pubkey}"
            else
                printf -- "- %-16s %-16s\n" "${i}" "${pubkey}"
            fi
        done
    else
        printf -- "- %s\n" "N/A"
    fi

    printf "\n"

    printf "%s\n" "[INFO] wg nodes:"
    if test "${nodes}"; then
        for i in ${nodes}; do
            pubkey=$(find "${WG_NODES_HOME}/${i}" -type f -iname *.pub -exec cat {} \;)
            if grep -qi "^#.*${pubkey}" "${WG_SERVER_HOME}/wg0.conf"; then
                printf -- "- %-16s %-16s (removed/disabled)\n" "${i}" "${pubkey}"
            else
                printf -- "- %-16s %-16s\n" "${i}" "${pubkey}"
            fi
        done
    else
        printf -- "- %s\n" "N/A"
    fi

    printf "\n"

    printf "%s\n" "[INFO] wg chains:"
    if test "${chains}"; then
        for i in ${chains}; do
            pubkey=$(find "${WG_CHAINS_HOME}/${i}" -type f -iname *.pub -exec cat {} \;)
            if grep -qi "^#.*${pubkey}" "${WG_SERVER_HOME}/wg0.conf"; then
                printf -- "- %-16s %-16s (removed/disabled)\n" "${i}" "${pubkey}"
            else
                printf -- "- %-16s %-16s\n" "${i}" "${pubkey}"
            fi
        done
    else
        printf -- "- %s\n" "N/A"
    fi
}

port="31337"
lan="0.0.0.0/0"
dns="8.8.8.8"

if test "${1}"; then
    check_deps
    action="${1}"
    shift
    case "${action}" in
        install-server)
            while getopts "p:ih" opts; do
                case "${opts}" in
                    p) port="${OPTARG}";;
                    i) interface="${OPTARG}";;
                    h) install_usage;;
                    *) install_usage;;
                esac
            done
            install_wg_server "${port}" "${interface}"
            ;;
        uninstall-server)
            uninstall_wg_server
            ;;
        add-peer)
            while getopts "s:n:p:d:h" opts; do
                case "${opts}" in
                    s) server="${OPTARG}"; validate_ip "${server}";;
                    n) name="${OPTARG}";;
                    p) port="${OPTARG}"; validate_port "${port}";;
                    d) dns="${OPTARG}"; validate_ip "${dns}";;
                    h) addpeer_usage;;
                    *) addpeer_usage;;
                esac
            done
            rand=$(printf "%s\n" "${RANDOM}" | md5sum | fold -w4 | head -1)
            name="${name:-${rand}}"
            add_peer
            ;;
        add-node)
            while getopts "s:i:n:p:l:h" opts; do
                case "${opts}" in
                    s) server="${OPTARG}"; validate_ip "${server}";;
                    i) interface="${OPTARG}";;
                    n) name="${OPTARG}";;
                    p) port="${OPTARG}"; validate_port "${port}";;
                    l) lan="${OPTARG}"; validate_cidr "${lan}";;
                    h) addnode_usage;;
                    *) addnode_usage;;
                esac
            done
            rand=$(printf "%s\n" "${RANDOM}" | md5sum | fold -w4 | head -1)
            name="${name:-${rand}}"
            add_node
            ;;
        add-chain)
            while getopts "s:n:p:h" opts; do
                case "${opts}" in
                    s) server="${OPTARG}"; validate_ip "${server}";;
                    n) name="${OPTARG}";;
                    p) port="${OPTARG}"; validate_port "${port}";;
                    h) addchain_usage;;
                    *) addchain_usage;;
                esac
            done
            rand=$(printf "%s\n" "${RANDOM}" | md5sum | fold -w4 | head -1)
            name="${name:-${rand}}"
            add_chain
            ;;
        remove-peer)
            while getopts "n:h" opts; do
                case "${opts}" in
                    n) name="${OPTARG}";;
                    h) removepeer_usage;;
                    *) removepeer_usage;;
                esac
            done
            remove_peer
            ;;
        remove-node)
            while getopts "n:h" opts; do
                case "${opts}" in
                    n) name="${OPTARG}";;
                    h) removenode_usage;;
                    *) removenode_usage;;
                esac
            done
            remove_node
            ;;
        remove-chain)
            while getopts "n:h" opts; do
                case "${opts}" in
                    n) name="${OPTARG}";;
                    h) removechain_usage;;
                    *) removechain_usage;;
                esac
            done
            remove_chain
            ;;
        list)
            list
            ;;
        status)
            wg show all
            ;;
        usage)
            usage
            ;;
        extusage)
            extended_usage
            ;;
        *)
            usage;;
    esac
else
    usage
fi
