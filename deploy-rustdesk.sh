#!/bin/bash

function usage() {
    printf "%s\n" \
        "usage: $(basename $0) [-s|--server] [-c|--client]" \
        "" \
        "-s|--server        downloads and installs rustdesk as server" \
        "-c|--client        downloads rustdesk versions for client access"
    exit 1
}

function install() {
    regex=()
    case $1 in
        server)
            regex+=(".*rustdesk-\d+\.\d+\.\d+-x86_64\.deb")
            for i in "${regex[@]}"; do
                latest=$(curl -sSL https://api.github.com/repos/rustdesk/rustdesk/releases | awk -F '"' '/browser_download_url/{print $4}' | grep -v nightly | grep -oP "${i}" | head -1)
                name=$(basename $latest)
                sudo rm -rf /tmp/rustdesk.deb
                curl -sSL $latest -o "/tmp/rustdesk.deb"
                printf "%s\n" "[INFO] downloading ${name}"
                sudo dpkg -i "/tmp/rustdesk.deb"
                printf "%s\n" "[INFO] installed ${name}"
                sudo rm -rf "/tmp/rustdesk.deb"
            done
            mkdir -p "${HOME}/.config/rustdesk" &>/dev/null
            cat > "${HOME}/.config/rustdesk/RustDesk2.toml" << EOF
nat_type = 1
serial = 0
unlock_pin = ''

[options]
verification-method = 'use-permanent-password'
enable-file-transfer = 'N'
enable-audio = 'N'
direct-server = 'Y'
enable-tunnel = 'N'
enable-record-session = 'N'
local-ip-addr = '$(ip r get 8.8.8.8 | grep -oP "src \K[^ ]+")'
EOF
            cat > "${HOME}/.config/rustdesk/RustDesk_default.toml" << EOF
[options]
show_quality_monitor = 'Y'
lock_after_session_end = 'Y'
view_style = 'original'
disable_audio = 'Y'
scroll_style = 'scrollbar'
image_quality = 'best'
EOF
            printf "%s\n" \
                "[INFO] set password for remote desktop with:" \
                "sudo rustdesk --password <password>"
            ;;
        client)
            regex+=(".*rustdesk-\d+\.\d+\.\d+-x86_64\.deb")
            regex+=(".*rustdesk-\d+\.\d+\.\d+-x86_64\.AppImage")
            regex+=(".*rustdesk-\d+\.\d+\.\d+-x86_64\.exe")
            regex+=(".*rustdesk-\d+\.\d+\.\d+-x86_64\.msi")
            
            mkdir -p "${HOME}/RustDesk" &>/dev/null
            for i in "${regex[@]}"; do
                latest=$(curl -sSL https://api.github.com/repos/rustdesk/rustdesk/releases | awk -F '"' '/browser_download_url/{print $4}' | grep -v nightly | grep -oP "${i}" | head -1)
                name=$(basename $latest)
                printf "%s\n" "[INFO] downloading ${name} to ${HOME}/RustDesk"
                curl -sSL $latest -o "${HOME}/RustDesk/${name}"
            done
            ;;
    esac
}

options=$(getopt -o sch --long server,client,help -n "$(basename $0)" -- "$@")
eval set -- "$options"
while true; do
    case "$1" in
        -s|--server) install "server"; break;;
        -c|--client) install "client"; break;;
        -h|--help)   usage; break;;
        *)           usage; break;;
    esac
    shift
done

