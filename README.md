### run remote script

- without args

```
ssh user@server [-p <port>] [-i <key>] '/bin/bash -s' < script.sh
```

- with args (e.g., getopts)

```
ssh user@server [-p <port>] [-i <key>] '/bin/bash -s -- -a test1 -b test2 -c' < script.sh
```

```
#!/bin/bash

third="asd"
while getopts "a:b:c" opts; do
    case $opts in
        a) first="${OPTARG}";;
        b) second="${OPTARG}";;
        c) third="test123";;
    esac
done

printf "%s\n" \
    "first is ${first}" \
    "second is ${second}" \
    "third is ${third}" | tee /tmp/test.txt
```
