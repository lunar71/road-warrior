#!/bin/bash
# -*- coding: utf8 -*-
set -e

test "${EUID}" -ne 0 && printf "%s\n" "run as root" && exit 1

usage() {
    printf "%s\n" "usage: $(basename $0) [ -l (listen only on localhost) ]"
    exit 1
}

while getopts "lh" opts; do
    case "${opts}" in
        l) action="localhost";;
        h) usage;;
        *) usage;;
    esac
done

if test "${action}" == "localhost"; then
    listen_addr="tcp://.:3389"
else
    listen_addr="tcp://:3389"
fi

export DEBIAN_FRONTEND=noninteractive 
apt-get update -y
apt-get install -y xrdp xorg tigervnc-xorg-extension tigervnc-standalone-server

cp /etc/xrdp/xrdp.ini /etc/xrdp/xrdp.ini.bak
sed -i 's/AllowRootLogin=.*/AllowRootLogin=false/g' /etc/xrdp/sesman.ini

base64 -d <<< 'Qk06AAAAAAAAADYAAAAoAAAAAQAAAAEAAAABABgAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAA////AA==' > /etc/xrdp/logo.bmp

cat > /etc/xrdp/xrdp.ini << EOF
[Globals]
# TCP send buffer size in bytes
# - Current: 33554432 (32 MB), matches sysctl net.ipv4.tcp_wmem max (33554432)
# - Purpose: Sets the buffer for outgoing RDP data (e.g., screen updates)
# - Reference: Should align with net.ipv4.tcp_wmem max in /etc/sysctl.conf
# - Lower Example: 1048576 (1 MB) - For moderate RDP use (e.g., 1080p) on slower systems/networks
tcp_send_buffer_bytes=33554432

# TCP receive buffer size in bytes
# - Current: 33554432 (32 MB), matches sysctl net.ipv4.tcp_rmem max (33554432)
# - Purpose: Sets the buffer for incoming RDP data (e.g., keyboard/mouse input)
# - Reference: Should align with net.ipv4.tcp_rmem max in /etc/sysctl.conf
# - Lower Example: 1048576 (1 MB) - For moderate RDP use (e.g., 1080p) on slower systems/networks
tcp_recv_buffer_bytes=33554432

ini_version=1
fork=true
port=${listen_addr}
use_vsock=false
tcp_nodelay=true
tcp_keepalive=true
security_layer=negotiate
crypt_level=medium
key_file=
ssl_protocols=TLSv1.2, TLSv1.3
autorun=
allow_channels=true
allow_multimon=true
bitmap_cache=true
bitmap_compression=true
bulk_compression=true
max_bpp=32
new_cursors=true
use_fastpath=both
grey=ffffff
black=555555
dark_grey=ffffff
blue=19315a
dark_blue=2777ff
white=eeeeee
#red=ff0000
#green=00ff00
#background=626c72
ls_title=Remote Desktop Protocol (xRDP)
ls_top_window_bg_color=2f2f2f
ls_width=350
ls_height=180
ls_bg_color=dedede
# convert wallpaper.png -type truecolor /etc/xrdp/wallpaper.bmp
# ls_background_image=/etc/xrdp/wallpaper.bmp
ls_logo_filename=/etc/xrdp/logo.bmp
ls_logo_x_pos=0
ls_logo_y_pos=0
ls_label_x_pos=30
ls_label_width=65
ls_input_x_pos=110
ls_input_y_pos=50
ls_input_width=210
ls_btn_ok_x_pos=142
ls_btn_ok_y_pos=135
ls_btn_ok_width=85
ls_btn_ok_height=30
ls_btn_cancel_x_pos=235
ls_btn_cancel_y_pos=135
ls_btn_cancel_width=85
ls_btn_cancel_height=30

[Logging]
LogFile=xrdp.log
LogLevel=INFO
EnableSyslog=true
#SyslogLevel=INFO
#EnableConsole=false
#ConsoleLevel=INFO
#EnableProcessId=false

[LoggingPerLogger]
#xrdp.c=INFO
#main()=INFO

[Channels]
rdpdr=true
drdynvc=true
cliprdr=true
rdpsnd=false
rail=false
xrdpvr=false
tcutils=false

[Xorg]
name=Xorg
lib=libxup.so
username=ask
password=ask
ip=127.0.0.1
port=-1
code=20

#[Xvnc]
#name=Xvnc
#lib=libvnc.so
#username=ask
#password=ask
#ip=127.0.0.1
#port=-1
#xserverbpp=24
#delay_ms=2000
#disabled_encodings_mask=0
EOF

mkdir -p /etc/polkit-1/rules.d &>/dev/null
cat > /etc/polkit-1/rules.d/50-xrdp-session.rules << EOF
polkit.addRule(function(action, subject) {
    if (subject.user && subject.user !== "root" &&
        (action.id == "org.freedesktop.color-manager.create-device" ||
         action.id == "org.freedesktop.color-manager.create-profile" ||
         action.id == "org.freedesktop.color-manager.delete-device" ||
         action.id == "org.freedesktop.color-manager.delete-profile" ||
         action.id == "org.freedesktop.color-manager.modify-device" ||
         action.id == "org.freedesktop.color-manager.modify-profile" ||
         action.id == "org.debian.apt.update-cache")) {
        return polkit.Result.YES;
    }
});

polkit.addRule(function(action, subject) {
    if (subject.user && subject.user !== "root" &&
        (action.id == "org.freedesktop.NetworkManager.settings.modify.system" ||
         action.id == "org.freedesktop.NetworkManager.network-control")) {
        return polkit.Result.YES;
    }
});

polkit.addRule(function(action, subject) {
    if (subject.user && subject.user !== "root" &&
        action.id.match(/^org\.freedesktop\.login1\.(reboot|power-off|suspend)/)) {
        if (subject.active) {
            return polkit.Result.YES;
        } else {
            return polkit.Result.NO;
        }
    }
});
EOF

if test -f /etc/sysctl.conf; then
    printf "%s\n" \
        "net.ipv4.tcp_wmem = 4096 262144 33554432" \
        "net.ipv4.tcp_rmem = 4096 262144 33554432" \
        "net.core.wmem_max = 33554432" \
        "net.core.rmem_max = 33554432" \
        "net.ipv4.tcp_window_scaling = 1" \
        "net.ipv4.tcp_fastopen = 3" \
        "net.core.netdev_max_backlog = 3000" \
        "net.core.somaxconn = 2048" \
        "net.ipv4.tcp_slow_start_after_idle = 0" \
        "net.ipv4.tcp_adv_win_scale = 1" \
        "net.core.default_qdisc = fq" \
        "net.ipv4.tcp_congestion_control = bbr" \
        >> /etc/sysctl.conf

    cat >> /etc/sysctl.conf << EOF
# Optimized TCP settings for single-user RDP (xrdp) over VPN on a 16 GB+ RAM system
# These settings enlarge buffers and improve throughput/latency for high-bandwidth,
# low-latency connections like a home LAN VPN, ensuring smooth Remote Desktop Protocol
# performance. Values are safe for a single-user setup with ample RAM.

# TCP send buffer sizes (min, default, max) in bytes
# - Min (4096): 4 KB, small enough for low-memory conditions
#   - Lower Example: 1024 (1 KB) - Use on very low-memory systems (e.g., <1 GB RAM) or embedded devices
#     where even 4 KB might strain resources under heavy load.
# - Default (262144): 256 KB, robust initial size for RDP sessions
#   - Lower Example: 16384 (16 KB) - Suitable for low-bandwidth connections (e.g., <10 Mbps) or
#     systems with limited RAM (e.g., 2-4 GB) where RDP traffic is light (e.g., text-based work).
# - Max (33554432): 32 MB, large buffer for graphical bursts (e.g., 4K, multi-monitor)
#   - Lower Example: 1048576 (1 MB) - Adequate for moderate RDP use (e.g., 1080p single monitor)
#     on slower VPNs (e.g., 20-50 Mbps) or systems with 4-8 GB RAM where memory conservation is key.
net.ipv4.tcp_wmem = 4096 262144 33554432

# TCP receive buffer sizes (min, default, max) in bytes
# - Matches tcp_wmem for symmetry, ensuring efficient two-way RDP traffic
net.ipv4.tcp_rmem = 4096 262144 33554432

# Maximum send buffer size system-wide (32 MB)
# - Caps individual socket buffers, aligning with tcp_wmem max
net.core.wmem_max = 33554432

# Maximum receive buffer size system-wide (32 MB)
# - Caps receive buffers, aligning with tcp_rmem max
net.core.rmem_max = 33554432

# Enable TCP window scaling
# - Allows dynamic buffer sizing for high-bandwidth links (1 = enabled)
net.ipv4.tcp_window_scaling = 1

# Enable TCP Fast Open (3 = client and server)
# - Reduces connection setup time, improving RDP responsiveness
net.ipv4.tcp_fastopen = 3

# Increase network device backlog queue
# - Handles more packets in queue (3000 > default 1000), good for bursty RDP traffic
net.core.netdev_max_backlog = 3000

# Increase socket listen backlog
# - Doubles default (2048 vs 128), ensures single-user bursts don’t drop connections
net.core.somaxconn = 2048

# Disable TCP slow start after idle
# - Faster recovery after idle periods (0 = disabled), keeps RDP snappy
net.ipv4.tcp_slow_start_after_idle = 0

# Optimize TCP window scaling factor
# - Sets scaling to favor throughput (1 = balanced), fine for VPN
net.ipv4.tcp_adv_win_scale = 1

# Set default queue discipline to Fair Queuing
# - Improves packet scheduling for RDP’s real-time needs
net.core.default_qdisc = fq

# Use BBR congestion control
# - Optimizes throughput/latency on VPN links with potential jitter
net.ipv4.tcp_congestion_control = bbr
EOF

    sysctl -p -q
fi

if command -v ufw &>/dev/null; then
    if test "${action}" == "localhost"; then
        ufw deny 3389/tcp
    else
        ufw allow 3389/tcp
    fi
    ufw --force enable
fi

systemctl enable --now -q xrdp xrdp-sesman
printf "%s\n" "xrdp installed and configured"
