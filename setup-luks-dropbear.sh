#!/bin/bash
# -*- coding: utf-8 -*-
set -e

[[ $EUID -ne 0 ]] && printf "%s\n" "run as root" && exit 1

DEBIAN_FRONTEND=noninteractive apt-get update -yqq
DEBIAN_FRONTEND=noninteractive apt-get install -yqq dropbear-initramfs

mkdir -p /etc/dropbear/initramfs &>/dev/null
cat > /etc/dropbear/initramfs/dropbear.conf << EOF
#
# Configuration options for the dropbear-initramfs boot scripts.
# You must run update-initramfs(8) to effect changes to this file (like
# for other files in the '/etc/dropbear/initramfs' directory).

#
# Command line options to pass to dropbear(8)
#
#DROPBEAR_OPTIONS=

#
# On local (non-NFS) mounts, interfaces matching this pattern are
# brought down before exiting the ramdisk to avoid dirty network
# configuration in the normal kernel.
# The special value 'none' keeps all interfaces up and preserves routing
# tables and addresses.
#
#IFDOWN=*

#
# On local (non-NFS) mounts, the network stack and dropbear are started
# asynchronously at init-premount stage.  This value specifies the
# maximum number of seconds to wait (while the network/dropbear are
# being configured) at init-bottom stage before terminating dropbear and
# bringing the network down.
# If the timeout is too short, and if the boot process is not blocking
# on user input supplied via SSHd (ie no remote unlocking), then the
# initrd might pivot to init(1) too early, thereby causing a race
# condition between network configuration from initramfs vs from the
# normal system.
#
#DROPBEAR_SHUTDOWN_TIMEOUT=60
DROPBEAR_OPTIONS="-I 180 -j -k -p 2222 -s -c cryptroot-unlock"
EOF

mkdir -p /root/luks-unlock
ssh-keygen -t rsa -b 4096 -f /root/luks-unlock/luks-unlock -C "" -q -N ""
cat /root/luks-unlock/luks-unlock.pub > /etc/dropbear/initramfs/authorized_keys
chmod 0600 /etc/dropbear/initramfs/authorized_keys
update-initramfs -u
update-grub

printf "\n"
printf "%s\n" "dropbear ssh private key is /root/luks-unlock/luks-unlock"
printf "%s\n" "NOTICE: use \"-o HostKeyAlgorithms=ssh-rsa\" when connecting via ssh"
printf "%s\n" "NOTICE: ensure static IP assignment via eth only, wifi drivers are not loaded during boot"
printf "\n"
