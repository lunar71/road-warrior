#!/bin/bash
set -e

function usage() {
    printf "%s\n" \
        "setup up simple command canaries for early intrusion detection via webhook notifications" \
        "usage: $(basename $0) <args>" \
        "" \
        "-c         required: commands to monitor" \
        "-u         required: webhook url" \
        "-m         required: method" \
        "-x         required: webhook headers" \
        "-d         required: webhook data" \
        "-h         print this help message and exit" \
        "" \
        "examples" \
        "" \
        "Slack:" \
        "$(basename $0) -c whoami -c id -c netcat \\
                        -u https://slack.com/api/chat.postMessage \\
                        -m POST \\
                        -x 'Content-Type: application/json' \\
                        -x 'Authorization: Bearer xoxb-a-b-c' \\
                        -d '{\"text\":\"**Command canary triggered**\\nHostname: \${HOSTNAME}\\nCommand: \${FUNCNAME[0]}\\nLocal time: \$(date --iso-8601=seconds)\", \"channel\":\"mychannel\"}'" \
        "" \
        "MS Teams:" \
        "$(basename $0) -c whoami -c id -c netcat \\
                        -u https://teams.com/webhook \\
                        -m POST \\
                        -x 'test1: test2' \\
                        -x 'test3: test4' \\
                        -d '{\"text\":\"**Command canary triggered**<br>Hostname: \${HOSTNAME}<br>Command: \${FUNCNAME[0]}<br>Local time: \$(date --iso-8601=seconds)\"}'"

    exit 1
}

if ! command -v curl &>/dev/null; then
    printf "%s\n" "[ERR] curl not found"
    exit 1
fi

commands=()
webhook_headers=()

while getopts "c:u:m:x:d:h" opts; do
    case "${opts}" in
        c) commands+=("${OPTARG}");;
        u) webhook_url="${OPTARG}";;
        m) method="${OPTARG}";;
        x) webhook_headers+=("${OPTARG}");;
        d) webhook_data="${OPTARG}";;
        h) usage;;
        *) usage;;
    esac
done

if test "${commands}" && test "${webhook_url}" && test "${webhook_headers}" && test "${webhook_data}" && test "${method}"; then
    curl_headers=""
    for i in "${webhook_headers[@]}"; do
        curl_headers+="-H '${i}' "
    done

    > canaries.txt
    for i in "${commands[@]}"; do
        data=$(printf "%s" "${webhook_data}" | sed -e 's/"/\\"/g')
        cat >> canaries.txt << EOF

function ${i}() {
    ($(which curl) -sSkL \\
        ${webhook_url} \\
        -X ${method} \\
        ${curl_headers} \\
        --data "${data}" \\
        -o /dev/null 2>&1 >/dev/null &)

    command ${i} "\${@}"
}

EOF
    done
    printf "%s\n" \
        "[INFO] successfully created canaries.txt" \
        "[INFO] add these to /etc/bash.bashrc"
    else
        usage
fi

